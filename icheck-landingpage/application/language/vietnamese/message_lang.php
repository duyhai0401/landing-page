<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['phone_is_exist']		= 'Số điện thoại đã tồn tại';
$lang['email_is_exist']		= 'Email đã tồn tại';
$lang['email_not_exist']		= 'Email không tồn tại';
$lang['permission_deny']	= 'Bạn không có quyền sử dụng chức năng này';


