<?php
defined('BASEPATH') OR exit('No direct script access allowed');


$lang['left_danh_muc'] = 'Danh mục nhân viên';
$lang['left_danh_sach_nhan_vien'] = 'Danh sách nhân viên';
$lang['left_danh_sach_vendor'] = 'Danh sách vendor';
$lang['left_danh_muc_qua'] = 'Danh mục quà';
$lang['left_tai_khoan_quan_tri'] = 'Tài khoản quản trị';
$lang['left_quan_tri_e_reward'] = 'QUẢN TRỊ E-RECOGNITION';
$lang['left_cap_diem_nam'] = 'Cấp điểm năm';
$lang['left_cap_diem_phat_sinh'] = 'Cấp điểm';
$lang['left_quan_ly_tang_diem'] = 'Quản lý tặng điểm';
$lang['left_quan_ly_doi_qua'] = 'Quản lý đổi quà';
$lang['left_thong_ke'] = 'Thống kê - báo cáo';
$lang['left_bao_cao_tang_diem'] = 'Báo cáo cấp điểm ';
$lang['left_bao_cao_nhan_diem'] = 'Báo cáo nhận điểm';
$lang['left_bao_cao_thu_hoi'] = 'Báo cáo thu hồi';
$lang['left_bao_cao_danh_muc_qua'] = 'Báo cáo danh mục quà';
$lang['left_quan_ly_frontend'] = 'Quản lý frontend';
$lang['left_menu'] = 'Menu';
$lang['left_slides'] = 'SlideShow';
$lang['left_green'] = 'Chương trình Hoa bia Xanh';
$lang['left_recognitions'] = 'Hoạt động khen thưởng khác';
$lang['left_title'] = 'Trang chủ';
$lang['left_tin_tuc'] = 'Tin tức & Sự kiện';
$lang['left_lich_su_nhan_diem'] = 'Lịch sử nhận điểm';
$lang['left_ket_noi_utop'] = 'Kết nối UTOP';
$lang['left_doi_diem'] = 'Đổi điểm';
$lang['left_quan_ly_doi_diem'] = 'Quản lý đổi điểm';
$lang['left_bao_cao_doi_diem'] = 'Báo cáo đổi điểm';

$lang['top_cai_dat_tai_khoan'] = 'Cài đặt tài khoản';
$lang['top_dang_xuat'] = 'Đăng xuất';

$lang['left_doi_qua'] = 'ĐỔI QUÀ';
$lang['left_tang_diem'] = 'Tặng điểm';
$lang['left_thu_hoi_diem'] = 'Thu hồi điểm tặng';
$lang['left_lich_su_cap_diem'] = 'LỊCH SỬ CẤP ĐIỂM';

$lang['top_tong_diem_tang'] = 'Tổng điểm có thể tặng';
$lang['top_tong_diem_duoc_tang'] = 'Tổng điểm được tặng';
$lang['top_tong_diem_doi_qua'] = 'Tổng điểm có thể đổi quà';
$lang['top_tong_diem_da_doi'] = 'Tổng điểm đã đổi';

