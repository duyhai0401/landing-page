<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$lang['messs_change'] = 'Thao tác này đã được thực hiện!';
$lang['btn_more_rog'] = 'Xem chi tiết';
$lang['btn_download_rog'] = 'Tải file';
$lang['title_green_home'] = 'Danh sách hoa bia xanh';
$lang['upload_image'] = 'Tải lên Ảnh ';
$lang['ph_action'] = 'Vui lòng chọn';
$lang['upload_image_avatar'] = 'Tải lên Ảnh avatar';
$lang['title_home'] = 'Trang chủ';
$lang['upload_image_file'] = 'Tải lên Ảnh chứng chỉ';
$lang['upload_file'] = 'Tải lên File';
$lang['title_vn'] = 'Tiêu đề trang tiếng Việt';
$lang['title_en'] = 'Tiêu đề trang tiếng Anh';
$lang['title_green'] = 'SALES - HOA BIA XANH';
$lang['title_recog'] = 'HOẠT ĐỘNG KHEN THƯỞNG KHÁC';
$lang['title_create_recog'] = 'Đăng tải Chương trình khen thưởng';
$lang['title_create_recog1'] = 'Nhập tên Chương trình';
$lang['title_create_recog2'] = 'Đường dẫn link tới bài viết';
$lang['title_create_recog3'] = 'Nhập Ảnh';
$lang['title_create_green'] = 'NHẬP DANH SÁCH HOA BIA XANH';
$lang['title_create_green1'] = 'Tháng';
$lang['btn_excell']			= 'Xuất file excel';
$lang['err_input']			= 'Không được để trống trường này!';
$lang['title_dd']			= 'Báo cáo đổi điểm';
$lang['btn_bg']			= 'Bản ghi';
$lang['employee_id']			= 'Mã nhân viên';
$lang['employee_mail']			= 'Email nhân viên';
$lang['employee_utop']			= 'Tài khoản utop';
$lang['title_pp']			= 'Tổng số điểm giao dịch thành công';
$lang['title_pp1']			= 'Tổng số điểm hoàn trả lại';
$lang['page_user_title']			= 'Danh sách nhân viên';
$lang['mudule_user_header_title']		= 'Danh mục nhân viên';
$lang['btn_login']	= 'Đăng nhập';
$lang['btn_logout']	= 'Đăng xuất';
$lang['btn_back']	= 'Trở lại';
$lang['user_name']	= 'Tên nhân viên';
$lang['user_branch']	= 'Địa điểm làm việc';
$lang['user_dep']	= 'Khối';
$lang['user_position']	= 'Chức vụ';
$lang['user_phone']	= 'Số điện thoại';
$lang['user_email']	= 'Email';
$lang['user_level']	= 'Cấp bậc';
$lang['user_avatar']	= 'Ảnh đại diện';
$lang['user_level_5']	= 'Nhân viên';
$lang['user_status']	= 'Trạng thái';
$lang['err_lg']	= 'Tên người dùng / kết hợp mật khẩu không chính xác / Tài khoản bị khóa';
$lang['btn_search']	= 'Tìm kiếm';
$lang['btn_dep']	= 'Phòng';
$lang['btn_create']	= 'Tạo';
$lang['btn_edit']	= 'Sửa';
$lang['btn_cancel']	= 'Hủy';
$lang['btn_close']	= 'Đóng';
$lang['search_by_name_point']	= 'Người nhận điểm';
$lang['user_id']	= 'ID';
$lang['edit_user']	= 'Sửa thông tin';
$lang['search_cn']	= 'Chi nhánh';
$lang['modal_mudule_awarded_title1']		= 'Chi tiết lịch sử tặng điểm';
$lang['delete']	= 'Xóa';
$lang['search_bdp']	= 'Lọc theo phòng ban';
$lang['create_single_acc']	= 'Thêm mới từng nhân viên';
$lang['create_list_acc']	= 'Cập nhật hàng loạt'; // them moi hang loat nhan vien
$lang['modal_feed_point_ex_body1']		= 'Số điểm cấp';
$lang['create_acc_success']	= 'Tạo tài khoản nhân viên thành công';
$lang['create_acc_fail']	= 'Tạo tài khoản nhân viên thất bại';

$lang['edit_acc_success']	= 'Sửa thông tin nhân viên thành công';
$lang['edit_acc_fail']	= 'Sửa thông tin nhân viên thất bại';

$lang['delete_acc_success']	= 'Xóa thông tin nhân viên thành công';
$lang['em_donate_point_send_mail']	= 'Gửi thông tin cho người khác cùng biết (vui lòng nhập email của người nhận. Nếu nhiều email, cách nhau bởi dấu phẩy)';

$lang['file_list_acc']	= 'File danh sách nhân viên';
$lang['can_not_read_file']	= 'File tải lên không thể đọc';

$lang['list_create_acc_success']	= 'Tạo tài khoản nhân viên thành công: ';
$lang['list_create_acc_fail']	= 'Tạo tài khoản nhân viên thất bại: ';
$lang['line']	= 'ở dòng';

$lang['modal_create_single_acc_title']	= 'THÊM MỚI NHÂN VIÊN';
$lang['modal_create_list_acc_title']	= 'Bạn đang tiến hành nhập hàng loạt danh sách nhân viên từ file gốc';
$lang['modal_create_list_acc_body_1']	= 'Để thực hiện được chức năng này, định dạng file gốc cần theo cấu hình các cột như file mẫu này';
$lang['modal_create_list_acc_body_2']	= 'Tải lên file exel có chứa danh sách toàn bộ nhân viên của bạn theo file mẫu';
$lang['modal_edit_acc_body']			= 'Bạn đang sửa thông tin cho ';
$lang['modal_delete_acc_title']			= 'Bạn đang xóa thông nhân viên ';
$lang['modal_delete_acc_body_1']		= 'Bạn đang xóa nhân viên khỏi danh sách nhân viên trên hệ thống. Việc này đồng nghĩa với việc nhân viên sẽ không thể truy cập vào hệ thống bằng tài khoản của họ nữa.';
$lang['modal_delete_acc_body_2']		= 'Bạn có chắc chắn với hành động này?';

$lang['modal_upload_file'] = 'Tải lên file danh sách';
$lang['download_file']	= 'Tải file mẫu';

$lang['make_sure']	= 'Chắc chắn';


///////////////////////////////////////////
$lang['mudule_vendor_title']			= 'Danh sách Vendor';
$lang['mudule_vendor_header_title']		= 'Danh sách Vendor';
$lang['create_vendor']					= 'Thêm Vendor';
$lang['vendor_name']					= 'Tên vendor';
$lang['create_vendor_success']			= 'Tạo tài khoản vendor thành công';
$lang['create_vendor_success_subject_email']	= '[Carlsberg Vietnam] e-ThankYou notification';
$lang['create_vendor_fail']				= 'Tạo tài khoản vendor thất bại';
$lang['edit_vendor_success']			= 'Sửa tài khoản vendor thành công';
$lang['edit_vendor_fail']				= 'Sửa tài khoản vendor thất bại';
$lang['delete_vendor_success']			= 'Xóa tài khoản vendor thành công';
$lang['lock_vendor_success']			= 'Khóa tài khoản thành công';
$lang['unlock_vendor_success']			= 'Mở khóa tài khoản thành công';
$lang['btn_create_vendor']				= 'thêm vendor';
$lang['btn_form_create_vendor']			= 'tạo vendor';
$lang['btn_lock_vendor']				= 'khóa đăng nhập';
$lang['btn_unlock_vendor']				= 'mở đăng nhập';
$lang['btn_delete_vendor']				= 'xóa vendor';
$lang['modal_edit_vendor_title']		= 'sửa thông tin vendor';
$lang['modal_edit_vendor_body']			= 'bạn đang sửa thông tin vendor ';
$lang['modal_lock_vendor_title']		= 'khóa vendor';
$lang['modal_lock_vendor_body']			= 'Bạn đang tiến hành khóa tài khoản đăng nhập của Vendor <br> Hành động này sẽ khiến Vendor không thể tiếp tục vào tài khoản <br> Bạn có chắc với hành động này?';
$lang['modal_unlock_vendor_title']		= 'Mở khóa vendor';
$lang['modal_unlock_vendor_body']		= 'Bạn đang tiến hành mở khóa tài khoản đăng nhập của Vendor ';
$lang['modal_delete_vendor_body']		= 'Bạn đang xóa Vendor khỏi hệ thống <br> Hành động này sẽ xóa bỏ cả tài khoản của Vendor trên hệ thống <br> Lịch sử liên quan tới Vendor sẽ được bảo lưu <br> Bạn có chắc chắc với hành động này?';
$lang['vendor_status_unlock'] 			= 'Hoạt động';
$lang['vendor_status_locked'] 			= 'Đang khóa';

$lang['search_by_name']	= 'Tìm theo tên';
$lang['search_by_name_send_point']	= 'Người được cấp điểm';
$lang['search_department']	= 'Khối';
$lang['search_branch']	= 'Địa điểm làm việc';
$lang['search_time_from']	= 'Thời gian từ';
$lang['search_time_to']	= 'Đến';
$lang['name_grant_point']	= 'Người Cấp Điểm';
$lang['name_granted_point']	= 'Người Được Cấp Điểm';
$lang['receiving_time']	= 'Thời Gian Nhận';
$lang['grant_point']	= 'Điểm được Cấp';
$lang['grant_point_send']	= 'Điểm Đã Tặng';
$lang['grant_point_rest']	= 'Điểm Còn Lại';
$lang['search']	= 'Lọc';
$lang['name_take']	= 'Người Nhận Điểm';
$lang['name_ng_cap']			= 'Tên người được cấp';
$lang['sum_point_chage']	= 'Tổng điểm đã đổi ';
$lang['sum_point_send']	= 'Tổng Điểm Được Tặng';
$lang['sum_point_rest']	= 'Tổng Điểm Còn Lại';
$lang['name_giff']	= 'Tên Quà';
$lang['original_number']	= 'Số Lượng Ban Đầu';
$lang['quantity_changed']	= 'Số Lượng Đã Đổi';
$lang['point_change']	= 'Số Điểm Đổi';
$lang['quantity_remaining']	= 'Số Lượng Còn';
$lang['title_giff_rp']	= 'THỐNG KÊ DANH MỤC QUÀ';
$lang['title_awarded_rp']	= 'BÁO CÁO CẤP ĐIỂM';
$lang['title_revoke_rp']	= 'Báo Cáo Thu Hồi Điểm';
$lang['title_recevice_rp']	= 'BÁO CÁO NHẬN ĐIỂM';
$lang['name_menu']	= 'Tên Menu';
$lang['image']	= 'Ảnh';
$lang['content']	= 'Nội Dung';
$lang['title_slider']	= 'QUẢN LÝ FRONTEND SLIDER';
$lang['title_new']	= 'Tin tức & Sự kiện';
$lang['title_menu']	= 'QUẢN LÝ FRONTEND MENU';
$lang['new_title']	= 'Title Bài Viết';
$lang['new_alias']	= 'ALIAS';
$lang['new_des']	= 'Des';
$lang['btn_add']	= 'Thêm';
$lang['btn_home']	= 'HOME';
$lang['btn_page']	= 'PAGES';
$lang['btn_revest']	= 'RESERVATION';
$lang['btn_element']	= 'ELEMENT';
$lang['btn_blog']	= 'BLOG';
$lang['btn_shop']	= 'SHOP';
$lang['btn_new']	= 'NEWS';
$lang['btn_other']	= 'OTHER';
$lang['btn_menu_can']	= 'Hủy';
$lang['btn_delete_menu']	= ' XOÁ MENU';
$lang['btn_delete_new']	= ' XOÁ TIN TỨC';
$lang['btn_delete_recog']	= ' XOÁ CORPORATE RECOGNITIONS';
$lang['btn_delete_green']	= ' XOÁ Green Hops Of The Month';
$lang['btn_delete_slide']	= ' XOÁ SLIDE';
$lang['btn_menu_sucss']	= 'Chắc chắn';
$lang['mess_delete_slide1']	= 'Bạn có chắc chắn với hành động này?';
$lang['mess_delete_menu1']	= 'Bạn có chắc chắn với hành động này?';
$lang['mess_delete_new1']	= 'Bạn có chắc chắn với hành động này?';
$lang['mess_delete_slide']	= ' Bạn đang xóa slide khỏi danh sách slide trên hệ thống.';
$lang['mess_delete_menu']	= ' Bạn đang xóa menu khỏi danh sách menu trên hệ thống.';
$lang['mess_delete_new']	= ' Bạn đang xóa menu khỏi danh sách menu trên hệ thống.';
$lang['mess_delete_green']	= ' Bạn đang xóa dữ liệu khỏi danh sách  trên hệ thống.';

$lang['mudule_gift_title']				= 'Danh sách quà';
$lang['mudule_gift_header_title']		= 'Danh sách quà';
$lang['gift_name']						= 'Tên quà';
$lang['gift_numb']						= 'Số lượng quà';
$lang['gift_point_value']				= 'Số điểm để đổi quà';
$lang['gift_vendor']					= 'Vendor';
$lang['gift_avatar']					= 'Ảnh sản phẩm';
$lang['create_gift_success']			= 'Tạo quà thành công';
$lang['create_gift_fail']				= 'Upload ảnh thất bại';
$lang['edit_gift_success']				= 'Sửa thông tin quà thành công';
$lang['edit_gift_fail']					= 'Sửa thông tin quà thất bại';
$lang['delete_gift_success']			= 'Xóa thông tin quà thành công';
$lang['btn_create_gift']				= 'Thêm quà mới';
$lang['btn_create_gift']				= 'Thêm quà mới';
$lang['gift_exist_numb']				= 'Số quà còn lại';
$lang['gift_show']						= 'Hiển thị';
$lang['modal_delete_gift_title']		= 'XOÁ QUÀ';
$lang['modal_delete_gift_body']			= 'Bạn có chắc chắn với hành động này?';
$lang['modal_create_gift_title']		= 'THÊM MỚI QUÀ';
$lang['modal_edit_gift_title']			= 'Sửa thông tin quà';

$lang['mudule_feed_point_title']		= 'Cấp điểm năm';
$lang['mudule_feed_point_header_title']	= 'Cấp điểm năm';
$lang['feed_point_lv1']					= 'Quỹ điểm cấp bậc 1';
$lang['feed_point_lv2']					= 'Quỹ điểm cấp bậc 2';
$lang['feed_point_lv3']					= 'Quỹ điểm cấp bậc 3';
$lang['feed_point_lv4']					= 'Quỹ điểm cấp bậc 4';
$lang['point']							= 'điểm';
$lang['btn_next']							= 'Xem Thêm';
$lang['message_feed_point_fail']		= 'Cấp điểm thất bại cho các tài khoản sau: <br>';
$lang['feed_point_success']				= 'Cấp điểm thành công.';
$lang['feed_point_limit']				= 'Chỉ được cấp điểm 1 lần trong 1 năm. Năm nay đã thực hiện cấp điểm.';
$lang['btn_feed_point']					= 'Cấp điểm';
$lang['feed_point_history']				= 'Lịch sử cấp điểm';
$lang['feed_point_time']				= 'Thời gian cấp điểm';
$lang['feed_point_manual']				= 'Người xử lý';
$lang['feed_point_history_lv1']			= 'Số điểm level 1';
$lang['feed_point_history_lv2']			= 'Số điểm level 2';
$lang['feed_point_history_lv3']			= 'Số điểm level 3';
$lang['feed_point_history_lv4']			= 'Số điểm level 4';
$lang['modal_feed_point_title']			= 'CẤP ĐIỂM THƯỞNG';
$lang['modal_feed_point_body']			= 'Bạn đang tiến hành cấp điểm thưởng năm 2020 cho cấp quản lý <br />Mỗi năm chỉ có thể sử dụng chức năng này một lần duy nhất<br />Bạn có chắc chắn ?';

$lang['mudule_feed_point_ex_title']		= 'Cấp điểm';
$lang['mudule_feed_point_ex_header_title']	= 'Cấp điểm';
$lang['point']							= 'Số điểm';
$lang['feed_point_ex_success']			= 'Cấp điểm phát sinh thành công';
$lang['feed_point_ex_seach_name']		= 'Tên/SĐT/Email';
$lang['feed_point_ex_manula']			= 'Người cấp điểm';
$lang['feed_point_ex_des_acc']			= 'Người được cấp điểm';
$lang['feed_point_ex_point']			= 'Số điểm được cấp';
$lang['modal_feed_point_ex_title']		= 'CẤP ĐIỂM PHÁT SINH TRONG NĂM';
$lang['modal_feed_point_ex_body']		= 'CẤP ĐIỂM PHÁT SINH TRONG NĂM';
$lang['modal_revoke_point_ex_title']		= 'THU HỒI ĐIỂM PHÁT SINH TRONG NĂM';
$lang['modal_revoke_point_ex_body']		= 'THU HỒI ĐIỂM PHÁT SINH TRONG NĂM CHO';

$lang['mudule_awarded_title']			= 'Quản lý tặng điểm';
$lang['mudule_awarded_header_title']	= 'Quản lý tặng điểm';
$lang['mudule_awarded_search_name']		= 'Tên người tặng';
$lang['mudule_awarded_time']			= 'Thời gian';
$lang['mudule_awarded_cur_acc']			= 'Người tặng';
$lang['mudule_awarded_cur_acc_id']			= 'ID Người tặng';
$lang['mudule_awarded_des_acc']			= 'Người nhận';
$lang['mudule_awarded_des_acc_id']			= 'ID Người nhận';
$lang['mudule_awarded_action']			= 'Hành vi';
$lang['mudule_awarded_des']			= 'Mô tả chi tiết lý do';
$lang['mudule_awarded_description']		= 'Thành tích';
$lang['modal_mudule_awarded_title']		= 'BIÊN NHẬN NHẬN QUÀ';

$lang['from_date']						= 'Thời gian từ';
$lang['to_date']						= 'Đến';
$lang['btn_filter']						= 'Lọc';

$lang['mudule_ex_gift_title']			= 'Quản lý đổi quà';
$lang['mudule_ex_gift_header_title']	= 'Quản lý đổi quà';
$lang['mudule_ex_gift_fullname']		= 'Tên người đổi';
$lang['mudule_ex_gift_fullname_']		= 'Người đổi quà';
$lang['mudule_ex_gift_quantum']			= 'Số lượng';
$lang['mudule_ex_gift_point_value']		= 'Số điểm đổi';
$lang['mudule_ex_gift_time']			= 'Thời gian đổi';
$lang['mudule_ex_gift_info']			= 'Thông tin';
$lang['mudule_ex_gift_bill']			= 'Biên nhận';
$lang['modal_ex_gift_title']			= 'BIÊN NHẬN NHẬN QUÀ';

$lang['mudule_ex_gift_reciver_name']	= 'Tên người nhận';
$lang['mudule_ex_gift_reciver_phone']	= 'Số điện thoại người nhận';

$lang['province']						= 'Tỉnh thành';
$lang['district']						= 'Quận Huyện';
$lang['ward']							= 'Xã phường/Thị trấn';
$lang['address']						= 'Địa chỉ';
$lang['address_detail']					= 'Số nhà/Thôn/Tổ xóm';

$lang['em_ex_gift_success']				= 'Tạo yêu cầu đổi quà thành công.';
$lang['em_ex_gift_fail']				= 'Tạo yêu cầu đổi quà thất bại.';
$lang['em_ex_gift_not_enough_point']	= 'Số điểm của bạn không đủ để đổi điểm.';
$lang['em_ex_gift_header_title']		= 'Chọn quà';
$lang['em_ex_gift_point_value']			= 'Số điểm để đổi:';
$lang['em_ex_gift_exist']				= 'Số lượng còn lại:';
$lang['em_ex_gift_choice']				= 'Bạn chọn quà';
$lang['em_ex_gift_total_point']			= 'Số điểm cần để đổi quà';
$lang['em_ex_gift_title_reciver_info']	= 'CUNG CẤP THÔNG TIN - ĐỊA CHỈ NGƯỜI NHẬN QUÀ';
$lang['em_ex_gift_reciver_address']		= 'Địa chỉ nhận';
$lang['em_ex_gift_history']				= 'Lịch sử đổi quà';
$lang['em_ex_gift_revice_time']			= 'Thời gian nhận';
$lang['em_gift_name']					= 'Thời gian nhận';
$lang['em_ex_gift_modal_title']			= 'XÁC NHẬN ĐỔI QUÀ';
$lang['em_ex_gift_modal_body1']			= 'Bạn tiến hành đổi điểm lấy quà là:';
$lang['em_ex_gift_modal_body2']			= 'Số lượng là:';
$lang['em_ex_gift_modal_body3']			= 'Số điểm dùng để đổi là:';
$lang['em_ex_gift_modal_body4']			= 'Người nhận là:';
$lang['em_ex_gift_modal_body5']			= 'Địa chỉ nhận là:';
$lang['em_ex_gift_modal_body6']			= 'Nêú chắc chắc hãy nhân Đổi quà, Nếu không hãy nhẩn Hủy';

$lang['em_btn_ex_gift']					= 'Đổi quà';

$lang['require_change_pass']			= 'Tài khoản của bạn đang sử dụng mật khẩu mặc định. Hãy đổi mật khẩu ngay ';

$lang['em_donate_point_title']			= 'Tặng điểm';
$lang['em_donate_point_header_title']	= 'Tặng điểm';
$lang['em_donate_point_reason']			= 'Lý do tặng';
$lang['em_donate_point_description']	= 'Mô tả chi tiết';
$lang['em_donate_point_success']		= 'Tặng điểm thành công!';
$lang['em_donate_point_search_title']	= 'BẠN MUỐN TẶNG ĐIỂM CHO AI?';
$lang['em_donate_point_search_choice']	= 'Lựa chọn';
$lang['em_donate_point_search_name']	= 'Họ tên';
$lang['em_donate_point_des_acc']		= 'NGƯỜI ĐƯỢC TẶNG';
$lang['em_donate_point_des_point']		= 'Chọn mức tuyên dương';
$lang['em_donate_point_lv1']			= 'Tốt';
$lang['em_donate_point_lv2']			= 'Rất tốt';
$lang['em_donate_point_lv3']			= 'Xuất sắc';
$lang['em_donate_point_lv4']			= 'Vượt trội';
$lang['em_donate_point_reason_acc']		= 'Hành vi';
$lang['em_donate_point_reason_desc']	= 'Mô tả chi tiết lý do';
$lang['btn_donate_point']				= 'Tặng điểm';
$lang['em_donate_point_history']		= 'LỊCH SỬ TẶNG ĐIỂM';
$lang['em_donate_point_tabl_time']		= 'Thời gian tặng';
$lang['em_donate_point_tabl_acc']		= 'Người được tặng';
$lang['em_donate_point_tabl_point']		= 'Số điểm tặng';
$lang['em_donate_point_modal_title']	= 'TẶNG ĐIỂM';
$lang['em_donate_point_modal_body1']	= 'Bạn đang tiến hành tặng điểm cho:';
$lang['em_donate_point_modal_body2']	= 'Số điểm tặng là:';
$lang['em_donate_point_modal_body3']	= 'Bạn chắc chắc muốn tặng điểm chứ?';

$lang['em_revoke_point_title']			= 'Thu hồi điểm';
$lang['em_revoke_point_header_title']	= 'Thu hồi điểm';
$lang['em_revoke_point_mess_fail']		= 'Thu hồi điểm thất bại cho các tài khoản sau: ';
$lang['em_revoke_point_success']		= 'Thu hồi điểm thành công.';
$lang['em_revoke_point_success2']		= 'Số điểm thu hồi lớn hơn số điểm nhân viên hiện có.';
$lang['em_revoke_point_search_title']	= 'BẠN MUỐN THU HỒI ĐIỂM ĐÃ TẶNG CHO AI?';
$lang['em_revoke_point_search_col1']	= 'Điểm tặng';
$lang['em_revoke_point_search_col2']	= 'Hành động';
$lang['em_revoke_point_history']		= 'LỊCH SỬ THU HỒI';
$lang['em_revoke_point_tabl_col1']		= 'Thời gian thu hồi';
$lang['em_revoke_point_tabl_col2']		= 'Điểm thu hồi';
$lang['em_revoke_point_modal_title']	= 'THU HỒI ĐIỂM';
$lang['em_revoke_point_modal_ng']	= 'Người Thu Hồi';
$lang['em_revoke_point_modal_ng1']	= 'Người Bị Thu Hồi';
$lang['em_revoke_point_modal_body1']	= 'Bạn đang tiến hành thu hồi điểm đã tặng cho:';
$lang['em_revoke_point_modal_body2']	= 'do tặng nhầm';
$lang['em_revoke_point_modal_body4']	= 'Bạn không thể thu hồi điểm do';
$lang['em_revoke_point_modal_body5']	= 'đã dùng điểm để đổi quà';
$lang['em_revoke_point_modal_body6']	= 'Bạn có chắc chắn muốn thu hồi điểm chứ?';

$lang['btn_revoke_point']				= 'Thu hồi điểm';

$lang['em_feed_point_rp_tabl_col1']		= 'Thời gian được cấp';

$lang['vendor_ex_gift_success']			= 'Thay đổi trạng thái thành công';
$lang['vendor_ex_gift_tabl_his']		= 'DANH SÁCH YÊU CẦU QUÀ';
$lang['vendor_ex_gift_filter_add']		= 'Lọc theo địa chỉ người nhận quà';
$lang['vendor_ex_gift_tabl_col1']		= 'All';
$lang['vendor_ex_gift_tabl_col2']		= 'Thời gian';
$lang['vendor_ex_gift_tabl_col3']		= 'quà yêu cầu';
$lang['vendor_ex_gift_total_peding']	= 'Quà chờ xủ lý ship';
$lang['vendor_ex_gift_total_success']	= 'Quà chuyển thành công';
$lang['vendor_ex_gift_total_shipping']	= 'Quà đang ship';

$lang['ex_gift_status_pending']			= 'Chờ xử lý';
$lang['ex_gift_status_success']			= 'Thành Công';
$lang['ex_gift_status_shipping']		= 'Đang ship';

$lang['btn_shiping']					= 'Đang ship';
$lang['btn_reviced']					= 'Đã nhận quà';

$lang['mudule_change_pass_old']			= 'Mật khẩu cũ';
$lang['mudule_change_pass_new']			= 'Mật khẩu Mới';
$lang['mudule_change_pass_re_new']		= 'Nhập lại mật khẩu mới';
$lang['mudule_change_pass_not_match']	= 'Mật khẩu nhập lại không đúng';
$lang['mudule_change_pass_worng_pas']	= 'Mật khẩu cũ không đúng';
$lang['mudule_change_pass_success']		= 'Thay đổi mật khẩu thành công';
$lang['mudule_change_pass_fail']		= 'Thay đổi mật khẩu thất bại';

$lang['mudule_acc_admin_title']			= 'Tài khoản quản trị';
$lang['mudule_acc_admin_header_title']	= 'Tài khoản quản trị';
$lang['mudule_acc_admin_create_success']= 'Tạo tài khoản quản trị thành công';
$lang['mudule_acc_admin_create_fail']	= 'Tạo tài khoản quản trị thất bại';
$lang['mudule_acc_admin_edit_success']	= 'Sửa khoản quản trị thành công';
$lang['mudule_acc_admin_edit_fail']		= 'Sửa tài khoản quản trị thất bại';
$lang['mudule_acc_admin_delete_success']= 'Xóa tài khoản quản trị thành công';
$lang['mudule_acc_admin_lock_success']	= 'Khóa tài khoản quản trị thành công';
$lang['mudule_acc_admin_unlock_success']= 'Mở khóa tài khoản quản trị thành công';
$lang['mudule_acc_admin_permission']	= 'Quyền hạn';
$lang['mudule_acc_admin_modal_title']	= 'Xóa tài khoản quản trị';
$lang['delete_acc_admin_modal_body1']	= 'Bạn đang xóa tài khoản quản trị trên hệ thống. Việc này đồng nghĩa với việc quản trị viên sẽ không thể truy cập vào hệ thống bằng tài khoản của họ nữa.';
$lang['lock_acc_admin_modal_title']		= 'Khóa tài khoản quản trị';
$lang['lock_acc_admin_modal_body1']		= 'Bạn đang khóa tài khoản quản trị trên hệ thống. Việc này đồng nghĩa với việc quản trị viên sẽ không thể truy cập vào hệ thống bằng tài khoản của họ nữa.';
$lang['unlock_acc_admin_modal_title']	= 'Mở khóa tài khoản quản trị';
$lang['unlock_acc_admin_modal_body']	= 'Bạn đang tiến hành mở khóa đăng nhập của tài khoản quản trị';

$lang['acc_admin_full_name']			= 'Tên quản trị viên';
$lang['btn_create_acc_admin']			= 'Thêm tài khoản quản trị';
$lang['btn_lock']						= 'Khóa';
$lang['btn_unlock']						= 'Mở khóa';
$lang['status_lock']					= 'Đang bị khóa';
$lang['status_active']					= 'Đang hoạt động';

/////////////
$lang['login_module_title']				= 'Đăng nhập';
$lang['login_username']					= 'Tên đăng nhập';
$lang['login_password']					= 'Mật khẩu';

$lang['no_data_found']					= 'Không tìm thấy dữ liệu';

$lang['fogot_password']					= 'Quên mật khẩu';
$lang['reset_password']					= 'Cấp lại mật khẩu';
$lang['subject_mail_reset_pass']		= 'Quên mật khẩu';
$lang['reset_pass_modal_body1']			= 'Một mail xác nhận đã được gửi tới email của bạn. Kiểm tra email và thực hiện theo hướng đẫn để thay đổi mật khẩu';
$lang['reset_pass_modal_body2']			= 'Thay đổi mật khẩu thất bại';
$lang['change_password']				= 'Thay đổi mật khẩu';
$lang['old_password']					= 'Mật khẩu cũ';
$lang['new_password']					= 'Mật khẩu mới';
$lang['re_new_password']				= 'Nhập lại mật khẩu mới';
$lang['active_link_not_found']			= 'Link xác nhận không tồn tại';
$lang['change_pass_success']			= 'Thay đổi mật khẩu thành công';

$lang['donate_point_modal_body1']		= 'Bạn cần chọn người được tặng và điền đầy đủ thông tin trước.';
$lang['donate_point_modal_title_noti']	= 'Thông báo';

$lang['btn_change_pass']				= 'Đổi mật khẩu';

$lang['em_ex_gift_not_enough_quantum']	= 'Số lượng quà không đủ.';

$lang['ex_gift_modal_body1']			= 'Bạn cần điền đầy đủ thông tin trước.';

$lang['cancel_ex_gift_modal_body1']		= 'Toàn bộ số điểm đã dùng để đổi quà sẽ được hoàn lại cho bạn.<br>Lệnh đổi quà cũng sẽ được xóa khỏi danh sách đổi quà.';
$lang['cancel_ex_gift_success']			= 'Hủy đổi quà thành công.';

$lang['vendor_ex_gift_modal_body1']		= 'Upload ảnh biên nhận';

$lang['reset_password_login_page']		= 'Quên mật khẩu';
$lang['btn_login_login_page']			= 'Đăng nhập';

$lang['create_admin_subject_email']		= '[Carlsberg Vietnam] e-ThankYou notification';
$lang['title_giff_list']		= 'DANH SÁCH QUÀ TẶNG';
$lang['title_top_point']		= 'TOP ĐIỂM CAO';
$lang['title_change_giff']		= 'HOẠT ĐỘNG ĐỔI QUÀ';
$lang['title_reward']		= 'HOẠT ĐỘNG E-ThankYou';
$lang['title_new_a_e']		= 'NEWS & EVENTS';

$lang['recieve_point_history_header_title']= 'Lịch sử nhận điểm';
$lang['recieve_point_history_tab_td_1']= 'Thời gian được nhận';
$lang['recieve_point_history_tab_td_2']= 'Số điểm được nhận';
$lang['recieve_point_history_tab_td_3']= 'Người tặng';
$lang['recieve_point_history_tab_td_4']= 'Lý do';

$lang['admin_donate_point_header_title']= 'Tặng điểm';
$lang['admin_donate_point_header_title_1']= 'Bạn muốn tặng điểm cho ai?';
$lang['admin_donate_point_header_title_2']= 'Nhập lý do tặng';
$lang['admin_donate_point_input_1']= 'Họ tên/Email';
$lang['admin_donate_point_td_1']= 'Lựa chọn';
$lang['admin_donate_point_td_2']= 'Họ và tên';

$lang['connect_utop_header_title'] = 'Kết nối UTOP';
$lang['btn_update']= 'Cập nhật';
$lang['connect_utop_input'] = 'Số điện thoại tài khoản UTOP';
$lang['connect_utop_info'] = 'Tài khoản UTOP hiện đang kết nối';
$lang['connect_utop_success'] = 'Cập nhật tài khoản UTOP thành công';
$lang['connect_utop_fail'] = 'Cập nhật tài khoản UTOP thất bại';

$lang['exchange_utop_point_header_title'] = 'Đổi điểm';
$lang['exchange_utop_point_value'] = 'Số điểm muốn đổi';
$lang['exchange_utop_point_history'] = 'Lịch sử đổi điểm';
$lang['exchange_utop_point_success'] = 'Bạn đã thực hiện đổi điểm. Vui lòng chờ xác nhận của HR admin';
$lang['exchange_utop_point_fail'] = 'bạn không đủ điểm để đổi';
$lang['exchange_utop_point_notyet_connect_utop'] = ' Bạn chưa kết nối với tài khoản UTOP. Vui lòng tải và cài đặt ứng dụng UTOP trên Google Play/ App Store và kết nối với hệ thống e-ThankYou trước khi tiến hành đổi điểm.';

$lang['exchange_point_rp_header_title'] = 'Quản lý đổi điểm';
$lang['exchange_point_rp_label_input1'] = 'Tên/SĐT/Email người đổi điểm';
$lang['exchange_point_rp_table_td1'] = 'Người đổi điểm';
$lang['exchange_point_rp_table_td2'] = 'Thao tác';
$lang['exchange_point_rp_status_pending'] = 'Chờ xác nhận';
$lang['exchange_point_rp_status_success'] = 'Thành công';
$lang['exchange_point_rp_status_fail'] = 'Thất bại';
$lang['exchange_point_rp_api_response_fail'] = 'Đổi điểm thất bại. Vui lòng thử lại sau';
$lang['exchange_point_rp_api_response_success'] = 'Đổi điểm thành công.';
$lang['exchange_point_rp_reject_response_success'] = 'Từ chối đổi điểm thành công.';
$lang['ex_gift_status_reject']		= 'Từ Chối';
$lang['btn_confirm'] = 'Xác nhận';
$lang['btn_reject'] = 'Từ chối';

$lang['donate_point_tooltip_1'] = 'Không thể tặng điểm cho nhân viên này.';
$lang['donate_point_fail'] = 'Tặng điểm thất bại!';

$lang['notification'] = 'Thông báo';

$lang['emplotee_id'] = 'ID';
$lang['user_gender'] = 'Giới tính';
$lang['user_gender_M'] = 'Nam';
$lang['user_gender_F'] = 'Nữ';
$lang['user_gender_O'] = 'Khác';
$lang['user_sub_dept'] = 'Phòng ban';
$lang['user_function'] = 'Bộ phận';

$lang['click_here'] = 'tại đây';
$lang['btn_next'] = 'Xem tiếp';

$lang['em_revoke_point_history_title_2'] = 'Lịch sử thu hồi';
$lang['STT'] = 'STT';
$lang['status_reject'] = 'Từ chối';

$lang['receive_point_rp_title'] = 'Thống kê Nhận Điểm';
$lang['select_all'] = 'Tất cả';
$lang['title_new1'] = 'Tin Tức';
$lang['title_new2'] = 'BÀI VIẾT GẦN ĐÂY';
$lang['title_new3'] = 'THEO DÕI';
$lang['title_id'] = 'ID người thu hồi';
$lang['title_id2'] = 'ID người bị thu hồi';
$lang['title_ng'] = 'Tên người thu hồi';
$lang['title_ng2'] = 'Tên người bị thu hồi';
$lang['btn_back_to_home'] = 'Trở về trang chủ';

$lang['em_donate_point_achievements'] = 'Thành tích đạt được';
$lang['em_donate_point_modal_body22'] = 'Nội dung tuyên dương';
$lang['em_donate_point_modal_body21'] = 'Mức độ tuyên dương';
$lang['achievements_1'] = 'Nhân viên hiểu rõ các mục tiêu của tập thể cũng như các ưu tiên/hành động của cá nhân nhằm đạt được mục tiêu chung';
$lang['achievements_2'] = 'Nhân viên chủ động phối hợp làm việc với các phòng ban, đối tác trong và ngoài công ty';
$lang['achievements_3'] = 'Nhân viên đảm bảo các quyết định đưa ra rõ ràng và được trao đổi kỹ, đồng thời xác định rõ kỳ vọng của bản thân để mọi người cùng hỗ trợ thực hiện các quyết định đó';
$lang['achievements_4'] = 'Nhân viên thể hiện sự cam kết, tính trách nhiệm cũng như kiên trì với các quyết định đưa ra';
$lang['achievements_5'] = 'Nhân viên lên kế hoạch để đảm bảo hiệu suất công việc, tối ưu thời gian và các nguồn lực cần thiết';
$lang['achievements_6'] = 'Nhân viên chịu trách nhiệm cá nhân đối với kết quả công việc, ngay cả khi nằm ngoài phạm vi trách nhiệm trực tiếp';
$lang['achievements_7'] = 'Nhân viên thực thi các quyết định có chất lượng theo đúng thời gian cam kết';
$lang['achievements_8'] = 'Nhân viên hành động nhanh để xử lý khắc phục khi công việc không đi đúng hướng';
$lang['achievements_9'] = 'Nhân viên hoàn tất toàn bộ kế hoạch đã đặt ra và giúp đỡ các thành viên trong nhóm cùng hoàn thành công việc của họ';

$lang['file_list_fb_acc']	= 'Danh sách cấp điểm cho nhân viên';
$lang['budget_point_fail_for_employee_id']	= 'Không thể cấp điểm theo file upload do mã nhân viên không hợp lệ';
$lang['budget_point_fail_for_point']	= 'Không thể cấp điểm theo file upload do điểm cấp không hợp lệ';
$lang['budget_point_list_success']	= 'Cấp điểm hàng loạt thành công.';
$lang['budget_point_list_modal_1']	= 'Bạn đang tiến hành cấp điểm hàng loạt từ file gốc';
$lang['budget_point_list_modal_2']	= 'Tải lên file exel có chứa danh sách cấp điểm cho nhân viên của bạn theo file mẫu';
$lang['budget_point_list_modal_body_1']	= 'Để thực hiện được chức năng này, định dạng file gốc cần theo cấu hình các cột như file mẫu này';
$lang['btn_budget_point_list']	= 'Cấp điểm hàng loạt';
$lang['btn_budget_point']	= 'Cấp điểm';
$lang['budget_point_download_template']	= 'Mẫu danh sách cấp điểm';
$lang['budget_point_upload_file']	= 'Danh sách cấp điểm cho nhân viên';


$lang['point_type']	= 'Loại điểm';
$lang['user_point_fund'] = 'Quỹ điểm';
$lang['invalid_email'] = 'Email không đúng định dạng';
$lang['hr_revoke_point_header_title'] = 'Thu hồi điểm tặng';

$lang['btn_like'] = 'Thích';
$lang['btn_liked'] = 'Thích';
$lang['btn_lock_comment'] = 'Khóa bình luận';
$lang['btn_unlock_comment'] = 'Mở khóa bình luận';
$lang['btn_post_comment'] = 'Đăng bình luận';
$lang['post_comment'] = 'Để lại bình luận';
$lang['total_like'] = "%s người đã thích điều này";
$lang['other_like'] = "Bạn và %s người khác đã thích điều này";
$lang['only_like'] = "Bạn đã thích điều này";
$lang['btn_delete_comment'] = "Xóa bình luận";
$lang['modal_body_delete_comment'] = "Bạn có chắc chắn muốn xóa bình luận này không?";
$lang['modal_body_unlock_comment'] = "Bạn có chắc chắn muốn mở khóa bình luận này không?";
$lang['modal_body_lock_comment'] = "Bạn có chắc chắn muốn khóa bình luận này không?";

$lang['limit_max_file_upload_size'] = "File tải lên phải nhỏ hơn 2MB";

$lang['revoke_rp_feed_point'] = "Điểm cấp";
$lang['revoke_rp_awarded_point'] = "Điểm tặng";

$lang['modal_delete_list_user_title'] = "Bạn đang thực hiện chức năng Xóa nhân viên hàng loạt";
$lang['modal_delete_list_user_body1'] = "Vui lòng nhập mã nhân viên tại đây để tìm kiếm nhân viên cần xóa, tối đa 10 nhân viên.";
$lang['modal_delete_list_user_body2'] = "Thông tin nhân viên:";
$lang['modal_delete_list_user_search_error'] = "Mã nhân viên không hợp lệ, vui lòng kiểm tra và thử lại sau.";
$lang['modal_delete_list_user_search_placeholder'] = 'Nhập mã nhân viên, mỗi mã cách nhau bằng dấu ","';
$lang['transaction_id'] = "Mã giao dịch";
$lang['write_comment_palaceholder'] = "Viết bình luận…";

$lang['search_extend'] = "Tìm kiếm nâng cao >>";
$lang['search_manual'] = "<< Tìm kiếm cơ bản";

$lang['revoke_fail_by_invalid_account'] = "Bạn không thể thu hồi điểm do nhân viên không còn tồn tại";
$lang['revoke_fail_by_invalid_amount'] = "Bạn không thể thu hồi điểm do %s đã dùng điểm để đổi quà";

$lang['number_ecognition_messages'] = "Số lượng thông điệp tuyên dương";
$lang['number_manage_using_etak'] = "Số lượng quản lý sử dụng e-ThankYou";
$lang['reminder_for_manage'] = "Lời nhắc cho người quản lý";
$lang['employees'] = "Nhân viên";
$lang['managers'] = "Quản lý";
$lang['hello'] = "Xin chào";
$lang['home_notice_recognition'] = "Bất cứ ai cũng muốn được ghi nhận, vậy nếu bạn muốn ghi nhận ai, đừng giữ điều đó là bí mật";
$lang['person_point_use'] = "Tỉ lệ điểm e-ThankYou được sử dụng";
$lang['person_point_use_by_function'] = "Số điểm e-ThankYou được sử dụng bởi các bộ phận";
$lang['home_recognition_history'] = "Lịch sử tuyên dương";
$lang['recognition_recieve_from'] = "vừa nhận một thông điệp tuyên dương từ";
$lang['home_released'] = "Đã sử dụng";
$lang['home_not_yet'] = "Chưa sử dụng";

$lang['btn_print'] = "In";

$lang['donate_point_fail_for'] = "Tặng điểm thất bại cho các tài khoản sau:";

$lang['can_not_comment'] = "Bạn không thể bình luận cho bài viết này";

$lang['modal_title_edit_recognitions'] = "Sửa chương trình trao thưởng";
$lang['modal_title_edit_green'] = "Sửa nhân viên của tháng";