<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require 'gg_cloud/autoload.php';

class Gg_sheet {
	
	public function gg_sheet_post($company_name, $tax, $phone, $utm_source, $utm_medium, $utm_campaign, $utm_id){
		$client = new \Google_Client();

		$client->setApplicationName('Google Sheets and PHP');

		$client->setScopes(Google_Service_Sheets::SPREADSHEETS);

		$client->setAccessType('offline');

		$client->setAuthConfig(APPPATH . 'libraries/vigilant-brace-320414-9ce35cd31080.json');

		$service = new Google_Service_Sheets($client);
		$spreadsheetId = '13HcWAN2QJQnJ9N-j-mx2jpwOGBcKbIzy-rOfER34NdU';
		$range = 'Sheet1';

       $get_range = "Sheet1";
	   $response = $service->spreadsheets_values->get($spreadsheetId, $get_range);

       $values = $response->getValues();
	   
	   $update_range = "Sheet1"; 
	   array_push($values, [$company_name, $tax, $phone, date('d-m-Y H:i:s'), $utm_source, $utm_medium, $utm_campaign, $utm_id]);
	   
	   //log_message('error', 'value_update ' . print_r($values, true));
	   $body = new Google_Service_Sheets_ValueRange([

		  'values' => $values

		]);

		$params = [

		  'valueInputOption' => 'RAW'

		];
	   
	   $update_sheet = $service->spreadsheets_values->update($spreadsheetId, $update_range, $body, $params);
	   log_message('error', 'UPdate gg sheet response ' . print_r($update_sheet, true));
	   
	}
}