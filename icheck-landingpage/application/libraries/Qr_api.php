<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class qr_api {
	
	public function post_curl($url, $data_string) {
		log_message('error', 'POST_CURL: '.$url);
		try
		{
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
			curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);           
			curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json; charset=utf-8'));

			$output = curl_exec($ch);
			$httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

			curl_close($ch);
			log_message('error', 'http code: ' . $httpcode);
			//log_message('error', 'output: ' . $output);
			if ($httpcode >= 200 && $httpcode < 300)
				return $output;
			else
			{
				return false;
			}
		} catch(Exception $e ) {
			log_message('error', 'Exceptions curl: ' . print_r($e, true));
			return false;
		}
    }
	
	public function get_curl($url, $alter = false) {
        log_message('error', 'GET_CURL: '.$url);
		try
		{
			
			
			$ch = curl_init($url);
			//curl_setopt($ch, CURLOPT_URL, $url);
			//curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			//curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
			//curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
			//curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
			//curl_setopt($ch, CURLOPT_TIMEOUT, 500);
			
			//curl_setopt($ch, CURLOPT_URL, $url);
			//curl_setopt($ch, CURLOPT_POST, 1);
			//curl_setopt($ch, CURLOPT_POSTFIELDS, "request=".$data_string);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);   

			$output = curl_exec($ch);
			$httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
			log_message('error', 'httpcode: ' . $httpcode);
			curl_close($ch);

			if ($httpcode >= 200 && $httpcode < 300){
				return $output;
			}
			else
			{
				return false;
			}
		} catch(Exception $e ) {
			log_message('error', 'Exceptions curl: ' . print_r($e, true));
			return false;
		}
    }
	
	public function get_otp_login($phone){
		$login_url = QR_API . '/login-otp/request?phoneNumber=' . $phone;
		$response = $this->get_curl($login_url);
		log_message('error', 'api get_otp_login response ' . print_r($response, true));
		if(!empty($response)){
			$response_data = json_decode($response);
			//log_message('error', 'api response ' . print_r($response_data, true));
			//if($response_data->code == '1'){
				return $response_data;
			//}
		}
		return false;
	}
	
	public function login_otp_confirm($otp, $token){
		$url = QR_API . '/login-otp/confirm';
		$data = ['token' => $token, 'otp' => $otp];
		$response = $this->post_curl($url, json_encode($data));
		log_message('error', 'api login_otp_confirm response ' . print_r($response, true));
		if(!empty($response)){
			$response_data = json_decode($response);
			//log_message('error', 'api response ' . print_r($response_data, true));
			//if($response_data->code == '1'){
				return $response_data;
			//}
		}
		return false;
	}
	
	public function get_checkin_history($customerId, $page=null, $page_size=null){
		$url = QR_API . '/checkin/' . $customerId . '/history';
		
		if($page){
			$url .= '?page=' . $page;
		}
		
		if($page_size){
			if(strpos($url, '?')){
				$url .= '&pageSize=' . $page;
			} else {
				$url .= '?pageSize=' . $page;
			}
		}
		
		$response = $this->get_curl($url);
		log_message('error', 'api get_checkin_history response ' . print_r($response, true));
		if(!empty($response)){
			$response_data = json_decode($response);
			//log_message('error', 'api response ' . print_r($response_data, true));
			//if($response_data->code == '1'){
				return $response_data;
			//}
		}
		return false;
	}
	
	public function get_checkin_history_by_token($token, $page=null, $page_size=null){
		$url = QR_API . '/checkin/history';
		
		if($page){
			$url .= '?page=' . $page;
		}
		
		if($page_size){
			if(strpos($url, '?')){
				$url .= '&pageSize=' . $page_size;
			} else {
				$url .= '?pageSize=' . $page_size;
			}
		}
		
		//$response = $this->get_curl($url);
		
		try
		{
			$authorization = "Authorization: Bearer $token";
			$ch = curl_init($url);
			curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json; charset=utf-8', $authorization));
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);   

			$output = curl_exec($ch);
			$httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
			log_message('error', 'httpcode: ' . $httpcode);
			curl_close($ch);

			if ($httpcode >= 200 && $httpcode < 300){
				$response = $output;
			}
			else
			{
				$response = false;
			}
		} catch(Exception $e ) {
			log_message('error', 'Exceptions curl: ' . print_r($e, true));
			$response = false;
		}
		
		log_message('error', 'api get_checkin_history response ' . print_r($response, true));
		if(!empty($response)){
			$response_data = json_decode($response);
			//log_message('error', 'api response ' . print_r($response_data, true));
			//if($response_data->code == '1'){
				return $response_data;
			//}
		}
		return false;
	}
	
	public function register_form_landing_page($phoneNumber, $password, $confirmPassword, $firstName, $lastName, $tax){
		$api_url = QR_API_REGISTER . '/landing-page/register/request';
		$data = ['phoneNumber' => $phoneNumber,
				'password' => $password,
				'confirmPassword' => $confirmPassword,
				'firstName' => $firstName,
				'lastName' => $lastName,
				'tax' => $tax];
				
		log_message('error', 'api register_form_landing_page request' . print_r($data, true));
		$response = $this->post_curl($api_url, json_encode($data));
		log_message('error', 'api register_form_landing_page response ' . print_r($response, true));
		if(!empty($response)){
			$response_data = json_decode($response);
			//log_message('error', 'api response ' . print_r($response_data, true));
			//if($response_data->status == '100' && isset($response_data->suggest_info)){
				return $response_data;
			//}
		}
		
		return false;
	}
	
	public function register_confirm($otp, $token){
		$api_url = QR_API_REGISTER . '/landing-page/register/confirm';
		$data = ['token' => $token,
				'otp' => $otp
				];
				
		log_message('error', 'api register_confirm request' . print_r($data, true));
		$response = $this->post_curl($api_url, json_encode($data));
		log_message('error', 'api register_confirm response ' . print_r($response, true));
		if(!empty($response)){
			$response_data = json_decode($response);
			//log_message('error', 'api response ' . print_r($response_data, true));
			//if($response_data->status == '100' && isset($response_data->suggest_info)){
				return $response_data;
			//}
		}
		
		return false;
	}
}