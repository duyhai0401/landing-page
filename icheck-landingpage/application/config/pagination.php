<?php
/**
 * Config pagination all module
 */
$config['per_page'] = 50;
$config['use_page_numbers'] = TRUE;
$config['num_links'] = 4;
$config['full_tag_open'] = '<ul class="pagination pagination-sm no-margin pull-right">';
$config['full_tag_close'] = '</ul>';
$config['num_tag_open'] = '<li class="fg-button ui-button ui-state-default">';
$config['num_tag_close'] = '</li>';
$config['cur_tag_open'] = '<li><a>';
$config['cur_tag_close'] = '</a></li>';
$config['next_tag_open'] = '<li class="fg-button ui-button ui-state-default next">';
$config['prev_tag_open'] = '<li class="fg-button ui-button ui-state-default previous">';
$config['first_tag_open'] = '<li class="fg-button ui-button ui-state-default first">';
$config['last_tag_open'] = '<li class="fg-button ui-button ui-state-default last">';
$config['next_link'] = 'Next';
$config['prev_link'] = 'Previous';
$config['first_link'] = 'First';
$config['last_link'] = 'Last';
$config['reuse_query_string'] = TRUE;
