<?php 

class MY_Controller extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
	}
	
	public function render_template($page = null, $data = array()) {
		//$this->load->model('model_auth');
		$session_data = $this->session->userdata();
		
		$data['menus'] = json_decode($session_data['menu'], true);
		//log_message('error', 'menu ' . print_r($data['menus'], true));
		$this->load->view('/template/header',$data);
		$this->load->view('/template/side_menubar',$data);
		//$this->load->view($view_type . 'templates/header_menu',$data);
		//$this->load->view('templates/side_menubar',$data);
		
		$this->load->view($page, $data);
		$this->load->view('/template/footer',$data);
	}
}
