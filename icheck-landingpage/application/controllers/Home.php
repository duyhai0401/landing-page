<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		
		//$this->load->library('register_api');
		
		if($_POST){
			if(isset($_POST['get_otp'])){
				$this->form_validation->set_rules('phone', 'Số điện thoại', 'trim|xss_clean|required');
				if ($this->form_validation->run() == true) {
					
					$this->load->library('qr_api');
					$get_otp_login = $this->qr_api->get_otp_login($this->input->post('phone'));
					if($get_otp_login && $get_otp_login->code == '1'){
						$this->session->set_userdata(['token' => $get_otp_login->token]);
						$message = "<p>OTP đã được gửi tới số điện thoại của bạn</p>";
						$this->session->set_flashdata('success', $message);
						$this->session->set_flashdata('otp_step', "OK");
					} else {
						if(isset($get_otp_login->message)){
							$error = '<p>' . $get_otp_login->message . '</p>';
						} else {
							$error = '<p>Vui lòng thử lại sau</p>';
						}
						$this->session->set_flashdata('error', $error);
					}
				} else {
					$error = validation_errors();
					$this->session->set_flashdata('error', $error);
				}
				redirect('qr-code-checkin');
			}
			
			if(isset($_POST['confirm_login'])){
				$this->form_validation->set_rules('otp', 'OTP', 'trim|xss_clean|required');
				if ($this->form_validation->run() == true) {
					$session_data = $this->session->userdata();
					$this->load->library('qr_api');
					$login_confirm = $this->qr_api->login_otp_confirm($this->input->post('otp'), $session_data['token']);
					if($login_confirm && $login_confirm->code == '1'){
						$login_data = $login_confirm->data;
						$session_data['id'] = $login_data->id;
						$session_data['token_b'] = $login_confirm->jwtToken;
						$this->session->set_userdata($session_data);
						// lay danh sach checkin
						$list_history = $this->qr_api->get_checkin_history_by_token($session_data['token_b']);
						if($list_history && $list_history->code == '1'){
							//var_dump($list_history);
							redirect('qr-code-checkin/lich-su');
						} else {
							if(isset($list_history->message)){
								$error = '<p>' . $list_history->message . '</p>';
							} else {
								$error = '<p>Vui lòng thử lại sau</p>';
							}
							$this->session->set_flashdata('error', $error);
						}
						
					} else {
						if(isset($login_confirm->message)){
							$error = '<p>' . $login_confirm->message . '</p>';
						} else {
							$error = '<p>Vui lòng thử lại sau</p>';
						}
						$this->session->set_flashdata('error', $error);
					}
				} else {
					$error = validation_errors();
					$this->session->set_flashdata('error', $error);
				}
				redirect('qr-code-checkin');
			}
		}
		
		$this->load->view('home/index');
	}
	
	public function history(){
		$this->load->library('qr_api');
		$session_data = $this->session->userdata();
		
		$total_records = 0;
		$limit_per_page = 10;
		
		$params['limit_per_page'] = $limit_per_page;
		$start_index =  (int)($this->uri->segment(3)) ? $this->uri->segment(3) : 1;
		//if($start_index > 0){
		//	$start_index = ($start_index -1)*$limit_per_page;
		//}
		if(isset($session_data['token_b'])){
			$list_history = $this->qr_api->get_checkin_history_by_token($session_data['token_b'], $start_index, $limit_per_page);
		} else {
			$list_history = false;
		}
		
		if($list_history && $list_history->code == '1'){
			$params["histories"] = $list_history->data;
			$total_records = $list_history->totalElement;
		}
		
		$config['base_url'] = base_url('qr-code-checkin/lich-su');
		$config['total_rows'] = $total_records;
		$config['per_page'] = $limit_per_page;
		$config["uri_segment"] = 3;
		
		$this->pagination->initialize($config);
		$params["links"] = $this->pagination->create_links();
		$params["page"] = $start_index;
		
		$this->load->view('home/history', $params);
		
	}
}
