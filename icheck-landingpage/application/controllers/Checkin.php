<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Checkin extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		if($_POST){
			if(isset($_POST['get_otp'])){
				$this->form_validation->set_rules('company', 'Tên Công Ty', 'trim|xss_clean|required');
				$this->form_validation->set_rules('tax', 'Mã số thuế', 'trim|xss_clean|required');
				$this->form_validation->set_rules('sodienthoai', 'Số điện thoại', 'trim|xss_clean|required');
				$this->form_validation->set_rules('password', 'Mật khẩu', 'trim|xss_clean|required');
				$this->form_validation->set_rules('check-password', 'Mật khẩu nhập lại', 'trim|xss_clean|required');
				if ($this->form_validation->run() == true) {
					
					$this->load->library('qr_api');
					$get_otp_login = $this->qr_api->register_form_landing_page($this->input->post('sodienthoai'), $this->input->post('password'),
																				$this->input->post('check-password'), $this->input->post('company'), "",
																				$this->input->post('tax'));
					if($get_otp_login && $get_otp_login->code == '1'){
						
						$user_data = ['company' => $this->input->post('company'),
									'tax' => $this->input->post('tax'),
									'phone' => $this->input->post('sodienthoai'),
									'token' => $get_otp_login->token];
						if($_GET){
							if(isset($_GET['utm_source'])){
								$user_data['utm_source'] = $_GET['utm_source'];
							}
							if(isset($_GET['utm_medium'])){
								$user_data['utm_medium'] = $_GET['utm_medium'];
							}
							if(isset($_GET['utm_campaign'])){
								$user_data['utm_campaign'] = $_GET['utm_campaign'];
							}
							if(isset($_GET['utm_id'])){
								$user_data['utm_id'] = $_GET['utm_id'];
							}
						}
						
						$this->session->set_userdata($user_data);
						
						//$this->session->set_flashdata('success', $message);
						$this->session->set_flashdata('otp_step', "OK");
					} else {
						if(isset($get_otp_login->message)){
							$error = '<p>' . $get_otp_login->message . '</p>';
						} else {
							$error = '<p>Vui lòng thử lại sau</p>';
						}
						$this->session->set_flashdata('error', $error);
					}
				} else {
					$error = validation_errors();
					$this->session->set_flashdata('error', $error);
				}
				redirect('checkin');
			}
			
			if(isset($_POST['confirm_otp'])){
				$this->form_validation->set_rules('otp', 'OTP', 'trim|xss_clean|required');
				if ($this->form_validation->run() == true) {
					$session_data = $this->session->userdata();
					$this->load->library('qr_api');
					$register_confirm = $this->qr_api->register_confirm($this->input->post('otp'), $session_data['token']);
					if($register_confirm && $register_confirm->code == '1'){
						//$message = "Đăng ký thành công";
						//$this->session->set_flashdata('success', $message);
						$this->load->library('gg_sheet');
						$session_data = $this->session->userdata();
						$this->gg_sheet->gg_sheet_post($session_data['company'], $session_data['tax'], $session_data['phone'], 
														isset($session_data['utm_source'])?$session_data['utm_source']:'',
														isset($session_data['utm_medium'])?$session_data['utm_medium']:'',
														isset($session_data['utm_campaign'])?$session_data['utm_campaign']:'',
														isset($session_data['utm_id'])?$session_data['utm_id']:'',
														);
						redirect('dang-ky-thanh-cong');
					} else {
						if(isset($register_confirm->message)){
							$error = '<p>' . $register_confirm->message . '</p>';
						} else {
							$error = '<p>Vui lòng thử lại sau</p>';
						}
						$this->session->set_flashdata('error', $error);
						$this->session->set_flashdata('otp_step', "OK");
					}
				} else {
					$error = validation_errors();
					$this->session->set_flashdata('error', $error);
					$this->session->set_flashdata('otp_step', "OK");
				}
				redirect('checkin');
			}
		}
		
		$this->load->view('checkin/index');
	}
	
	public function success(){
		$this->load->view('checkin/success');
	}

	public function create()
	{
		if($_POST){
			if(isset($_POST['get_otp'])){
				$this->form_validation->set_rules('company', 'Tên Doanh Nghiệp', 'trim|xss_clean|required');
				$this->form_validation->set_rules('full_name', 'Họ tên', 'trim|xss_clean|required');
				$this->form_validation->set_rules('sodienthoai', 'Số điện thoại', 'trim|xss_clean|required');
				$this->form_validation->set_rules('email', 'Email', 'trim|xss_clean|required');
				if ($this->form_validation->run() == true) {
					
					$this->load->library('qr_api');
					$get_otp_login = $this->qr_api->register_form_landing_page($this->input->post('sodienthoai'), "112323","112323", $this->input->post('company'), "","");
					if($get_otp_login && $get_otp_login->code == '1'){
						
						$user_data = ['company' => $this->input->post('company'),
									'phone' => $this->input->post('sodienthoai'),
									'full_name' => $this->input->post('full_name'),
									'email' => $this->input->post('email'),
									'token' => $get_otp_login->token];
						if($_GET){
							if(isset($_GET['utm_source'])){
								$user_data['utm_source'] = $_GET['utm_source'];
							}
							if(isset($_GET['utm_medium'])){
								$user_data['utm_medium'] = $_GET['utm_medium'];
							}
							if(isset($_GET['utm_campaign'])){
								$user_data['utm_campaign'] = $_GET['utm_campaign'];
							}
							if(isset($_GET['utm_id'])){
								$user_data['utm_id'] = $_GET['utm_id'];
							}
						}
						
						$this->session->set_userdata($user_data);
						
						//$this->session->set_flashdata('success', $message);
						$this->session->set_flashdata('otp_step', "OK");
					} else {
						if(isset($get_otp_login->message)){
							$error = '<p>' . $get_otp_login->message . '</p>';
						} else {
							$error = '<p>Vui lòng thử lại sau</p>';
						}
						$this->session->set_flashdata('error', $error);
					}
				} else {
					$error = validation_errors();
					$this->session->set_flashdata('error', $error);
				}
				redirect('checkin/create');
			}
			
			if(isset($_POST['confirm_otp'])){
				$this->form_validation->set_rules('otp', 'OTP', 'trim|xss_clean|required');
				if ($this->form_validation->run() == true) {
					$session_data = $this->session->userdata();
					$this->load->library('qr_api');
					$register_confirm = $this->qr_api->register_confirm($this->input->post('otp'), $session_data['token']);
					if($register_confirm && $register_confirm->code == '1'){
						$message = "Đăng ký thành công";
						$this->session->set_flashdata('success', $message);
						$this->load->library('gg_sheet');
						$session_data = $this->session->userdata();
						$this->gg_sheet->gg_sheet_post($session_data['company'], $session_data['full_name'], $session_data['email'], $session_data['phone'], 
														isset($session_data['utm_source'])?$session_data['utm_source']:'',
														isset($session_data['utm_medium'])?$session_data['utm_medium']:'',
														isset($session_data['utm_campaign'])?$session_data['utm_campaign']:'',
														isset($session_data['utm_id'])?$session_data['utm_id']:'',
														);
						redirect('dang-ky-thanh-cong');
					} else {
						if(isset($register_confirm->message)){
							$error = '<p>' . $register_confirm->message . '</p>';
						} else {
							$error = '<p>Vui lòng thử lại sau</p>';
						}
						$this->session->set_flashdata('error', $error);
						$this->session->set_flashdata('otp_step', "OK");
					}
				} else {
					$error = validation_errors();
					$this->session->set_flashdata('error', $error);
					$this->session->set_flashdata('otp_step', "OK");
				}
				redirect('checkin/create');
			}
		}
		$this->load->view('checkin/idp');
	}
	
}
