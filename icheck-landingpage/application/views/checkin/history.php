<!DOCTYPE html> 
<html lang="en">
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta charset="utf-8">
		
		<title>icheck | landing</title>
		<link rel="stylesheet" href="//fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic" id="style-resource-3">
		<link rel="stylesheet" href="<?php echo base_url() .  'assets/css/bootstrap.css' ?>" id="style-resource-4">
		
		<link rel="stylesheet" href="<?php echo base_url() .  'assets/css/custom.css' ?>" id="style-resource-8">
		<link rel="stylesheet" href="<?php echo base_url() . 'assets/js/owl_carousel/owl.carousel.min.css' ?>">
		<link rel="stylesheet" href="<?php echo base_url() . 'assets/css/animate.min.css' ?>">
		<script src="<?php echo base_url() . 'assets/js/jquery-1.11.3.min.js' ?>"></script> 
	  
	</head>
<body class="page-body page-fade" >
	<div class="container-fluid header-bg" style="padding-left: 0;padding-right: 0;">
		
			<div class="container">
				<div class="row" >
					<div class="col-md-2 col-sm-2">
						<img class="header-logo" src="<?php echo base_url() . 'assets/images/logo.png' ?>" />
					</div>
					<div class="col-md-10 col-sm-10 text-right">
						<a class="qr-tutorial" >Hướng dẫn quét mã check-in</a>
						<button class="btn icheck-btn destination-lookup">Tra cứu điểm đến</button>
					</div>
				</div>
				
				<div class="row" >
					<div class="col-md-5 col-sm-12 checkin">
						<p class="checkin-title-1">Check in ngay,</p>
						<p class="checkin-title-2">nhận quà liền tay</p>
						<ul>
							<li>Check in bằng mã QR tại điểm đến, đảm bảo an toàn mùa dịch <br>
							<li>Không giới hạn cơ hội nhận quà khi check-in </li>
						</ul>
					</div>
					<div class="col-md-7 col-sm-12">
						<img class="checkin-qr-img" src="<?php echo base_url() . 'assets/images/checkin-qr-img.png' ?>" />
					</div>
				</div>
				
				
			</div>
		
		<div id="fixedContainer"></div>
		
	</div>
	
	<div class="container-fluid" style="padding-top: 80px;padding-bottom: 60px;">
		<div class="container">
			<div class="row ">
				<div class="col-md-6 col-sm-6 col-md-offset-3 col-sm-offset-3 clearfix advantages-checkin" >Lịch sử QR check-in</div>
			</div>
			<div class="row ">
				<div class="col-12 bg-advantages-title"></div>
			</div>
			<div class="row " style="margin-top: 50px;">
				<table class="table table-bordered responsive">
					<thead>
					<tr>
						<th>STT</th>
						<th>Tên </th>
						<th>Địa chỉ</th>
						<th>Ngày</th>
					</tr>
					</thead>
					<tbody>
						<?php if(isset($histories) && $histories): ?>
						<?php // var_dump($histories); ?>
						<?php $stt = 1 ?>
						<?php foreach($histories as $key => $items): ?>
							<?php foreach($items->data as $data ): ?>
							<tr>
								<td><?php echo $stt * $page ?></td>
								<td><?php echo $data->name ?></td>
								<td><?php echo $data->address ?></td>
								<td><?php echo $data->createdAt ?></td>
							</tr>
							<?php $stt++ ?>
							<?php endforeach; ?>
						<?php endforeach; ?>
						<?php endif; ?>
					</tbody>
				</table>
						
				<?php echo $links; ?>
			</div>
			
		</div>
	</div>
		
	<div class="container-fluid footer" >
		<div class="container">
			<div class="row">
				<div class="col-md-6 col-sm-6">
					<div class="logo-footer"></div>
					<p class="icheck-name">Công ty cổ phần iCheck</p>
					
					<div class="col-md-6 col-sm-6 footer-qrcode">
					</div>
					
					<div class="col-md-6 col-sm-6">
						<div class="appstore"></div>
						<div class="ggplay"></div>
					</div>
					
				</div>
				<div class="col-md-6 col-sm-6">
					<p class="branch-name">Văn phòng miền Bắc</p>
					<div class="branch-address">
						<div class="icon-location" ></div>
						<p>Tầng 12 Tòa nhà Diamond Flower, số 48 Lê Văn Lương, Khu đô thị mới N1, Phường Nhân Chính, Quận Thanh Xuân, Hà Nội</p>
					</div>
					
					<p class="branch-name" style="padding-top: 25px;">Văn phòng miền Nam</p>
					<div class="branch-address">
						<div class="icon-location" style="width: 27px;"></div>
						<p>Số 7/1 Thành Thái phường 14 quận 10, Tp Hồ Chí Minh</p>
					</div>
					
					<div style="width: 100%; display: inline-flex;padding-top: 10px;">
						<p class="phone-mail-contect"><img style="margin-right: 12px;" src="<?php echo base_url() . 'assets/images/icon-phone.png' ?>"></i>0974 195 488</p>
						<p class="phone-mail-contect"><img style="margin-right: 12px;" src="<?php echo base_url() . 'assets/images/icon-mail.png' ?>"></i>cskh@icheck.com.vn</p>
					</div>
					
				</div>
			</div>
		</div>
		
		<div class="bg-virus-right"></div>
	</div>
	  
	  
	<script src="<?php echo base_url() . 'assets/js/bootstrap.js" id="script-resource-3' ?>"></script> 
	<script src="<?php echo base_url() . 'assets/js/jquery-ui/js/jquery-ui-1.10.3.minimal.min.js' ?>" id="script-resource-2"></script> 
	<script src="<?php echo base_url() . 'assets/js/owl_carousel/owl.carousel.min.js' ?>"></script>
	<script src="<?php echo base_url() . 'assets/js/bootstrap-notify.min.js' ?>"></script>
	
	<?php $error = $this->session->flashdata('error');
		if($error){
			$message = $error;
		}
		$success = $this->session->flashdata('success');
		if($success){
			$message = $success;
		}
		if(isset($message)){
			$message = str_replace("\n", '', $message);
		}
	?>
	<script>
		$(document).ready(function() {
			/*
			$('.featured-carousel').owlCarousel({
				loop:true,
				autoplay: true,
				margin:30,
				animateOut: 'fadeOut',
				animateIn: 'fadeIn',
				nav:false,
				dots: true,
				autoplayHoverPause: false,
				items: 1,
				
				responsive:{
				  0:{
					items:1
				  },
				  600:{
					items:1
				  },
				  1000:{
					items:1
				  }
				}
			});
			*/
			
			$(".qr-tutorial").click(function() {
				$([document.documentElement, document.body]).animate({
					scrollTop: $("#tutorial-qr-checkin").offset().top
				}, 1500);
			});
			
			$(".destination-lookup").click(function() {
				$([document.documentElement, document.body]).animate({
					scrollTop: $("#section-login").offset().top
				}, 1500);
			});
			
			<?php if(!empty($message)): ?>
			$([document.documentElement, document.body]).animate({
					scrollTop: $("#section-login").offset().top
				}, 1500);
			$.notify({
				icon: 'glyphicon glyphicon-bell',
				message: <?php echo "'" . $message . "',"; ?>
				title: 'Thông báo',
			},{
				type:  <?php echo ($error)?"'danger'":"'success'" ?>,
				delay: 5000,
			});
			<?php endif; ?>
		});
	</script>
	
</body>
</html>