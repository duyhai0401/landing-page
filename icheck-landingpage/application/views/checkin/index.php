<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title><?php echo WEB_TITLE ?></title>
    <link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url('assets/images/icon-title.jpg') ?>" />
	
	<link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap.min.css?v=' . VERSION_WEB)    ?>" id="style-resource-4">
    <link rel="stylesheet" href="<?php echo base_url('assets/css/landingpage.css?v=' . VERSION_WEB) ?>" id="style-resource-8">
	<link rel="stylesheet" href="<?php echo base_url('assets/js/owl_carousel/owl.carousel.min.css?v=' . VERSION_WEB) ?>" id="style-resource-6">
	
	<link rel="stylesheet" href="<?php echo base_url('assets/font-awesome-4.7.0/css/font-awesome.css?v=' . VERSION_WEB) ?>" id="style-resource-5">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/animate.min.css?v=' . VERSION_WEB) ?>">
	
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@400;500;700;900&display=swap" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
</head>
<body>
<div class="qd-landingpage-wraper">
    <div class="section-1">
        <div class="container">
            <div class="row">
                <div class="col">
                    <nav class="navbar navbar-expand-lg navbar-light">
                        <a class="navbar-brand" href=""><img src="<?php echo base_url() ?>assets/images/new_landing_page/logo.png"></a>
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-icon"><i class="fa fa-bars" aria-hidden="true"></i></span>
                        </button>
                        <div class="collapse navbar-collapse" id="navbarNav">
                            <ul class="navbar-nav ml-auto">
								<li class="nav-item">
                                    <a class="nav-link" href="#tinh-nang">Tính năng</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#huong-dan">Hướng dẫn</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="https://marketing.icheck.vn">Đăng nhập</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#form-register">Trải nghiệm miễn phí</a>
                                </li>
                            </ul>
                        </div>
                    </nav></div>

                <div class="w-100"></div>
                <div class="banner1 col-xl-7 col-lg-6 text-center">
                    <img src="<?php echo base_url() ?>assets/images/new_landing_page/banner.png" class="banner-1"/>
                </div>
                <div class="col-lg-6 col-xl-5 feature-list-wrapper">
                    <h3 class="giai-phap">Giải pháp Check-in thông minh bằng mã QR</h3>
                    <ul class="feature-list">
                        <li>Hạn chế tiếp xúc, đảm bảo an toàn cho nhân viên và khách hàng</li>
                        <li>Phát hiện nguy cơ lây nhiễm kịp thời</li>
                        <li>Hỗ trợ doanh nghiệp 100% chi phí</li>
                    </ul>
                    <div class="row">
                        <div class="col-12 col-lg-6 btn-watch-video">
                            <a href="https://youtu.be/dnMNUfSqJ28"> <i class="fa fa-play-circle-o" aria-hidden="true"></i> XEM VIDEO</a>
                        </div>
                        <div class="col-12 col-lg-6 dung-thu-wrapper">
                            <a href="#form-register" class="btn-dung-thu">DÙNG THỬ NGAY</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="section-2">
            <div class="container">
                <h3 class="title-section-2">Tại sao doanh nghiệp nên sử dụng QR Check-in?</h3>
                <div class="row">
                    <div class="col-lg-4 shield-item">
                        <img src="<?php echo base_url() ?>assets/images/new_landing_page/shield-1.png" title="bảo vệ nhân viên và khách hàng"/>
                        <div class="shield-card">
                            <div class="shield-title">Bảo vệ nhân viên và<br>khách hàng</div>
                            <span class="shield-description">Hạn chế tiếp xúc thông qua check-in <br>bằng cách quét mã</span>
                        </div>
                    </div>
                    <div class="col-lg-4 shield-item">
                        <img src="<?php echo base_url() ?>assets/images/new_landing_page/shield-2.png" title="Quản lý dữ liệu khách hàng tới điểm bán"/>
                        <div class="shield-card">
                            <div class="shield-title">Quản lý dữ liệu khách hàng <br>tới điểm bán</div>
                            <span class="shield-description">Ghi nhận chính xác thời gian check-in và thông tin khách hàng khi quét mã</span>
                        </div>
                    </div>
                    <div class="col-lg-4 shield-item">
                        <img src="<?php echo base_url() ?>assets/images/new_landing_page/shield-3.png" title="Tiết kiệm thời gian check-in, nâng cao trải nghiệm khách hàng"/>
                        <div class="shield-card">
                            <div class="shield-title">Tiết kiệm thời gian check-in, nâng cao trải nghiệm khách hàng</div>
                            <span class="shield-description">Khách hàng check-in tự động,<br>không cần chờ đợi</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <div class="section-3" id="tinh-nang">
        <h3 class="title-section-3">Khám phá tính năng nổi bật của QR Check-in</h3>
        <div class="container-fluid container-feature">
            <div class="row">
                <div class="col-lg-4 featured-item">
                    <img src="<?php echo base_url() ?>assets/images/new_landing_page/feature-1.png" title="check-in không điểm chạm"/>
                    <div class="feature-title">
                        Check-in<br>không điểm chạm
                    </div>
                </div>
                <div class="col-lg-4 featured-item">
                    <img src="<?php echo base_url() ?>assets/images/new_landing_page/feature-2.png" title="Quản lý dữ liệu  khách hàng tới điểm bán"/>
                    <div class="feature-title">
                        Quản lý dữ liệu<br>khách hàng tới điểm bán
                    </div>
                </div>
                <div class="col-lg-4 featured-item">
                    <img src="<?php echo base_url() ?>assets/images/new_landing_page/feature-3.png" title="Tích hợp chương trình Loyalty"/>
                    <div class="feature-title">
                        Tích hợp<br>chương trình Loyalty
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row justify-content-center">
                <div class="cta-feature-section">
                    <div class="cta-feature-text">
                        <h2>Sẵn sàng để bắt đầu?</h2>
                        <span>Trải nghiệm QR Check-in để tự tin chống dịch</span>
                    </div>
                    <a href="#form-register" class="cta-button">Dùng thử ngay</a>
                </div>
            </div>
        </div>
    </div>
    <div class="section-4" id="huong-dan">
        <div class="container">
            <div class="row justify-content-center video-qr-wrapper">
                <div class="hdsd-wrapper col-lg-8 col-12 align-self-center">
                    <img src="<?php echo base_url() ?>assets/images/new_landing_page/hdsd.png" title="Hướng dẫn sử dụng qr checkin"/>
                    <a href="https://youtu.be/dnMNUfSqJ28" class="view-video"><i class="fa fa-play-circle-o" aria-hidden="true"></i> XEM VIDEO</a>
                </div>
            </div>
            <div class="row customer-wrapper">
                <div class="col-lg-5">
                    <h3>Khách hàng tiêu biểu <br>của QR Check-in</h3>
                </div>
                <div class="col-lg-7">
                    <div class="logo-partner">
                        <div><img src="<?php echo base_url() ?>assets/images/new_landing_page/logo-soibien.png" /></div>
                        <div><img src="<?php echo base_url() ?>assets/images/new_landing_page/logo-anvui.png" /></div>
                        <div><img src="<?php echo base_url() ?>assets/images/new_landing_page/logo-phongvu.png" /></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="section-5" id="form-register">
        <div class="container">
            <div class="row">
                <div class="col-lg-7 left-contain-wrapper">
                    <h3>Trải nghiệm miễn phí<br> Giải pháp Check-in thông minh</h3>
                    <ul class="solution-list">
                        <li>Hạn chế tiếp xúc, đảm bảo an toàn cho nhân viên và khách hàng</li>
                        <li>Phát hiện nguy cơ lây nhiễm kịp thời</li>
                        <li>Hỗ trợ doanh nghiệp 100% chi phí</li>
                    </ul>
                </div>
                <div class="col-lg-5">
                    <div class="cf7-frm-wrapper">
						<?php
							$current_url = current_url();
							if($_SERVER['QUERY_STRING']){
								$current_url .= '?' . $_SERVER['QUERY_STRING'];
							}
						?>
                        <form action="<?php echo $current_url ?>" method="post" class="wpcf7-form init" novalidate="novalidate" data-status="init">
							<div style="display: none;">
								<input type="hidden" name="_wpcf7" value="3157">
								<input type="hidden" name="_wpcf7_version" value="5.3.2">
								<input type="hidden" name="_wpcf7_locale" value="en_US">
								<input type="hidden" name="_wpcf7_unit_tag" value="wpcf7-f3157-o1">
								<input type="hidden" name="_wpcf7_container_post" value="0">
								<input type="hidden" name="_wpcf7_posted_data_hash" value="">
							</div>
							<p>
								<span class="wpcf7-form-control-wrap company">
									<input type="text" name="company" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required form-control" aria-required="true" aria-invalid="false" placeholder="Tên Công Ty *">
								</span>
								<br>
								<span class="wpcf7-form-control-wrap tax">
									<input type="text" name="tax" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required form-control" aria-required="true" aria-invalid="false" placeholder="Mã số thuế *">
								</span>
								<br>
								<span class="wpcf7-form-control-wrap sodienthoai">
									<input type="text" name="sodienthoai" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required form-control" aria-required="true" aria-invalid="false" placeholder="Số điện thoại *">
								</span>
							</p>
							<div class="wpcf7-form-control-qd-password"> 
								<div class="wpcf7-form-control-wrap">
									<input type="password" value="" name="password" placeholder="Mật khẩu" class="form-control">
								</div>
								<div class="wpcf7-form-control-wrap">
									<input type="password" value="" name="check-password" placeholder="Nhập lại mật khẩu" class="form-control">
								</div>
							</div>
							<input type="submit" name="get_otp" value="Đăng ký ngay" class="wpcf7-form-control wpcf7-submit"><p></p>
							<div class="wpcf7-response-output" aria-hidden="true"></div>
						</form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer">
        <div class="container">
            <div class="row">
                <div class="col-12 col-lg-5 footer-left-wrapper">
                    <img src="https://icheck.com.vn/wp-content/uploads/2021/05/logo.svg" class="logo">
                    <p>Công ty cổ phần iCheck</p>
                    <div class="get-app row">
                        <div class="get-app-qr-code">
                            <img src="https://icheck.com.vn/wp-content/themes/icheck/assets/images/footer_qr.png"  class="qrcode"/>
                        </div>
                        <div class="get-app-store">
                            <a href="https://apps.apple.com/app/id1001036590" data-wpel-link="external" rel="nofollow external noopener noreferrer"><noscript><img src="https://icheck.com.vn/wp-content/themes/icheck/assets/images/app_store.svg" alt="" loading="lazy"></noscript><img class=" lazyloaded" src="https://icheck.com.vn/wp-content/themes/icheck/assets/images/app_store.svg" data-src="https://icheck.com.vn/wp-content/themes/icheck/assets/images/app_store.svg" alt="" loading="lazy"></a>
                            <a href="https://play.google.com/store/apps/details?id=vn.icheck.android&amp;hl=vi" data-wpel-link="external" rel="nofollow external noopener noreferrer"><noscript><img src="https://icheck.com.vn/wp-content/themes/icheck/assets/images/google_play.svg" alt="" loading="lazy"></noscript><img class=" lazyloaded" src="https://icheck.com.vn/wp-content/themes/icheck/assets/images/google_play.svg" data-src="https://icheck.com.vn/wp-content/themes/icheck/assets/images/google_play.svg" alt="" loading="lazy"></a>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-lg-7 footer-right-wrapper" >
                    <ul class="address">
                        <li>
                            <p class="office">Văn phòng miền Bắc</p>
                            <p>Tầng 12 Tòa nhà Diamond Flower, số 48 Lê Văn Lương, Khu đô thị mới N1, phường Nhân Chính, quận Thanh Xuân, Hà Nội</p>
                        </li>
                        <li>
                            <p class="office">Văn phòng Hồ Chí Minh</p>
                            <p>Số 7/1 Thành Thái phường 14 quận 10, Tp Hồ Chí Minh</p>
                        </li>
                    </ul>
                    <div class="contact">
                        <span>
                            <i class="fa fa-phone" aria-hidden="true"></i>0902 195 488 - 0974 195 488
                        </span>
                        <span>
                            <i class="fa fa-envelope" aria-hidden="true"></i>cskh@icheck.com.vn
                        </span>
                    </div>
                    <div class="get-app-store-bottom">
                        <a href="https://apps.apple.com/app/id1001036590" data-wpel-link="external" rel="nofollow external noopener noreferrer"><noscript><img src="https://icheck.com.vn/wp-content/themes/icheck/assets/images/app_store.svg" alt="" loading="lazy"></noscript><img class=" lazyloaded" src="https://icheck.com.vn/wp-content/themes/icheck/assets/images/app_store.svg" data-src="https://icheck.com.vn/wp-content/themes/icheck/assets/images/app_store.svg" alt="" loading="lazy"></a>
                        <a href="https://play.google.com/store/apps/details?id=vn.icheck.android&amp;hl=vi" data-wpel-link="external" rel="nofollow external noopener noreferrer"><noscript><img src="https://icheck.com.vn/wp-content/themes/icheck/assets/images/google_play.svg" alt="" loading="lazy"></noscript><img class=" lazyloaded" src="https://icheck.com.vn/wp-content/themes/icheck/assets/images/google_play.svg" data-src="https://icheck.com.vn/wp-content/themes/icheck/assets/images/google_play.svg" alt="" loading="lazy"></a>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
<?php $otp_step = $this->session->flashdata('otp_step'); ?>
<?php if($otp_step): ?>
	<div class="modal fade" id="otp_confirm">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header" style="background-color: #238A3E;">
					<h4 class="modal-title" style="color: #fff;">Xác nhận đăng ký</h4>
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button> 
				</div>
				
				<div class="modal-body">
					<div class="row">
						<p style="width: 80%;text-align: center;margin: 0 auto;margin-bottom: 20px;">
							OTP đã được gửi tới số điện thoại của bạn. Nhập OTP để xác nhận đăng ký
						</p>
						<form class="form-inline" style="width: 100%;" method="post" action="<?php echo base_url(uri_string()) ?>">
						<div style="margin: 0 auto;margin-bottom: 20px; width: 100%;text-align: center;">
							<input class="form-control" name="otp" placeholder="Nhập OTP" style="margin-right: 15px;max-width: 60%;margin: 0 auto;">
							<button type="submit" name="confirm_otp" class="btn btn-default" style="background-color: #238A3E; color: #fff;">Xác nhận</button> 
						</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php endif; ?>

	<script src="<?php echo base_url('assets/js/owl_carousel/owl.carousel.min.js?v=' . VERSION_WEB) ?>"></script> 
	<script src="<?php echo base_url('assets/js/bootstrap.min.js?v=' . VERSION_WEB) ?>" id="script-resource-3"></script> 
	<script src="<?php echo base_url('assets/js/bootstrap-notify.min.js?v=' . VERSION_WEB) ?>"></script>
	<?php $error = $this->session->flashdata('error');
		if($error){
			$message = $error;
		}
		$success = $this->session->flashdata('success');
		if($success){
			$message = $success;
		}
		if(isset($message)){
			$message = str_replace("\n", '', $message);
		}
		
	?>
	<script>
    $(document).ready(function(){
		<?php if($otp_step): ?>
		$('#otp_confirm').modal({backdrop: 'static', keyboard: false});
		<?php endif; ?>
        //$('.cf7-frm-wrapper .wpcf7-submit').on('click',function(event){
            event.preventDefault();
            //var target = $(event.target);
            //var data = {
            //    "phoneNumber": $('.wpcf7-form-control-wrap input[name="sodienthoai"]').val(),
            //    "password": $('.wpcf7-form-control-wrap input[name="password"]').val(),
            //    "confirmPassword": $('.wpcf7-form-control-wrap input[name="check-password"]').val(),
            //    "firstName": $('.wpcf7-form-control-wrap input[name="company"]').val(),
            //    "lastName": $('.wpcf7-form-control-wrap input[name="company"]').val(),
            //    "tax": $('.wpcf7-form-control-wrap input[name="tax"]').val()};
            //console.log(data);
			//
            //$.ajax({
            //    type: "post",
            //    dataType: "json",
            //    contentType: 'application/json',
            //    url: "https://qrmkt-api.dev.icheck.vn/qr-code-free/api/v1/landing-page/register/request",
            //    data:JSON.stringify(data),
            //    success: function (response) {
            //        if (response.code == 1) {
            //            $('.cf7-frm-wrapper form').submit();
            //            setTimeout(function(){ window.location.replace("<?php echo site_url('/success/') ?>"); }, 500);
            //        } else {
            //            alert(response.message)
            //        }
            //    },
            //})
        //})
		
		<?php if($error && !isset($otp_step)): ?>
			$([document.documentElement, document.body]).animate({
				scrollTop: $("#form-register").offset().top
			}, 1500);
		<?php endif; ?>
		
		<?php if(!empty($message)): ?>
			
		$.notify({
			icon: 'glyphicon glyphicon-bell',
			message: <?php echo "'" . $message . "',"; ?>
			title: 'Thông báo',
		},{
			type:  <?php echo ($error)?"'danger'":"'success'" ?>,
			delay: 5000,
		});
		<?php endif; ?>
		
    })
	</script>
	
	
	<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-WFK9B3D');</script>
<!-- End Google Tag Manager -->

<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-WFK9B3D"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
</body>
</html>


