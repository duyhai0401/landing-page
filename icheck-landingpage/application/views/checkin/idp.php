<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css"
    integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title><?php echo WEB_TITLE ?></title>
    <link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url('assets/images/icon-title.jpg') ?>" />
  <link rel="stylesheet" href="<?php echo base_url('assets/css/css/index.css') ?>">
  <link rel="stylesheet" href="<?php echo base_url('assets/css/css/common.css') ?>">
  <link rel="preconnect" href="https://fonts.googleapis.com">
  <link rel="stylesheet" href="<?php echo base_url('assets/font-awesome-4.7.0/css/font-awesome.css?v=' . VERSION_WEB) ?>" id="style-resource-5">
    <link rel="stylesheet" href="<?php echo base_url('assets/css/animate.min.css?v=' . VERSION_WEB) ?>">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@400;500;700;900&display=swap" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
</head>

<body>

  <!-- baner -->
  <div class="baner">
    <div class="baner_main h-100">
      <div class="baner_content container">
        <div class="d-flex justify-content-between">
          <div class="logo flex-basis-50">
           <a href="<?php echo base_url() ?>"> <img src="<?php echo base_url() ?>assets/images/img/logo_icheck.png" alt=""></a>
          </div>
          <div class="baner_navbar flex-basis-50 ">
            <nav class="navbar navbar-expand-lg navbar-light text-uppercase font-weight-bold">
              <div class="collapse navbar-collapse justify-content-end" id="navbarTogglerDemo03">
                <ul class="navbar-nav mt-2 mt-lg-0 justify-content-between">
                  <li class="nav-item active d-block pr-3 pl-3">
                    <a class="nav-link text-white" href="#service">Dịch vụ <span class="sr-only">(current)</span></a>
                  </li>
                  <li class="nav-item d-block pr-3 pl-3">
                    <a class="nav-link text-white" href="#use">Lợi ích</a>
                  </li>
                  <li class="nav-item d-block pr-3 pl-3">
                    <a class="nav-link text-white" href="#sale">Ưu đãi</a>
                  </li>
                  <li class="nav-item d-block pr-3 pl-3">
                    <a class="nav-link text-white" href="#promo">Tư vấn miễn phí</a>
                  </li>
                </ul>
              </div>
              <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo03"
                aria-controls="navbarTogglerDemo03" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
              </button>
            </nav>
          </div>
        </div>
        <div class="pt-5">
          <p class="text-uppercase text-center baner-main_title font-weight-bold pt-5 pb-5">
            đăng ký mã số mã vạch
          </p>
          <div class="background-gradiant-green baner-sub_title text-white w-50 text-center">
            <p class="font-size-35">Nhận mã doanh nghiệp</p>
          </div>
          <div class="background-gradiant-green baner-sub_title text-white w-50 text-center">
            <p class="text-uppercase font-weight-bold font-size-35">Chỉ trong <span class="color-yellow font-size-35">1
                NGÀY</span></p>
          </div>
        </div>
        <div class="baner-content text-white pt-5">
          <ul class="baner-content_text">
            <li class="li1">Chi phí thấp hơn đến 40% đối so với các nhà cung cấp khác</li>
            <li class="mrleft-10">Miễn phí kiểm nghiệm sản phẩm</li>
          </ul>
        </div>
      </div>
    </div>
    <button type="button" class="btn text-uppercase btn-more_info btn-warning btn-orange font-weight-bold btn-lg">Tìm
      hiểu thêm</button>
  </div>

  <!-- cause and effect -->
  <div class="cause-effect pt-5">
    <div class="cause-effect_main container">
      <div class="cause-effect_title font-weight-bold text-center">
        <span class="d-block  font-size-40">Hành trình gian nan</span>
        <span class="d-block  font-size-40">Tự đăng ký mã số mã vạch</span>
      </div>
      <p class="cause-effect_title_cause text-center color-green font-weight-bold font-size-30 mt-5">
        Doanh nghiệp đau đầu vì
      </p>
      <div class="cause-content mt-5">
        <div class="row">
          <div class="col-md-4 col-12">
            <div class="background-orange border-radius-20 p-4 cause-content_item mt-2">
              <span>Có quá nhiều thủ tục quy định, chi phí ẩn phát sinh khi đăng ký mã số mã vạch sản phẩm</span>
            </div>
          </div>
          <div class="col-md-4 col-12">
            <div class="background-orange border-radius-20 p-4 cause-content_item mt-2">
              <span>Doanh nghiệp thiếu kinh nghiệm và nhân sự chuyên trách để hoàn thiện giấy tờ, thủ tục khiến thời
                gian
                kiểm định, đăng ký mã kéo dài</span>
            </div>
          </div>
          <div class="col-md-4 col-12">
            <div class="background-orange border-radius-20 p-4 cause-content_item mt-2">
              <span>Dịch vụ đăng ký hộ nhan nhản, nhưng không tìm được đơn vị uy tín, có kinh nghiệm hỗ trợ doanh
                nghiệp</span>
            </div>
          </div>
        </div>
      </div>
      <div class="row effect mt-5">
        <div class="col-md-6 col-12 pb-5">
          <span class="font-size-30 color-green font-weight-bold">
            Hậu quả
          </span>
          <div class="d-flex align-items-center mt-4">
            <div class="mr-3 m-auto-0 pr-3 mt-1">
              <img src="<?php echo base_url() ?>assets/images/img/icon_attention.png" alt="">
            </div>
            <div>
              Sản phẩm ra mắt mãi không có mã vạch, không vào được các chuỗi phân phối lớn
            </div>
          </div>
          <div class="d-flex align-items-center mt-3">
            <div class="mr-3 m-auto-0 pr-3 mt-1">
              <img src="<?php echo base_url() ?>assets/images/img/icon_attention.png" alt="">
            </div>
            <div>
              Sản phẩm thiếu mã vạch bị người tiêu dùng nghi ngờ về chất lượng và nguồn gốc xuất xứ
            </div>
          </div>
          <div class="d-flex align-items-center mt-3">
            <div class="mr-3 m-auto-0 pr-3 mt-1">
              <img src="<?php echo base_url() ?>assets/images/img/icon_attention.png" alt="">
            </div>
            <div>
              Doanh nghiệp khó kiểm soát, quản lý sản phẩm
            </div>
          </div>

        </div>
        <div class="col-md-6 col- 12">
          <div class="effect-image h-100">
          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- Benefit -->
  <div class="benefit">
    <div class="container">
      <div class="row">
        <div class="col-md-6 col- 12">
          <div>
            <img src="<?php echo base_url() ?>assets/images/img/group_barcode.png" alt="">
          </div>
          <p class="font-weight-bold font-size-25 mt-4 text-white">
            Mọi lo lắng sẽ được giải quyết khi bạn tìm đúng chuyên viên, đúng đơn vị tư vấn có tâm có tầm
          </p>
          <p class="text-yellow font-size-35 mt-5 font-weight-bold mb-0">
            iCheck ở đây để giúp bạn!
          </p>
          <span class="font-size-18 text-white">
            iCheck là đơn vị cung cấp dịch vụ đăng ký mã vạch toàn diện
          </span>
          <div class="mt-3">
           <a href="#promo"> <button type="button" class="btn  text-uppercase btn-warning btn-orange font-weight-bold btn-lg">Đăng ký
              ngay</button></a>
          </div>
        </div>
        <div class="col-md-6 col- 12">
          <div class="text-white">
            <p class="font-weight-bold font-size-25 mt-4">
              Lợi ích khi đăng ký mã số mã vạch với iCheck
            </p>
            <div class="border-bottom-white pt-3 pb-3 d-flex ">
              <div class="mr-3 m-auto-0 pr-3 benefit-icon">
                <img src="<?php echo base_url() ?>assets/images/img/icon_time.png" alt="">
              </div>
              <div>
                <span class="d-block font-weight-bold">
                  Rút ngắn thời gian đăng ký và nhận mã
                </span>
                <span>
                  Doanh nghiệp sẽ nhận được mã số mã vạch chậm nhất sau 24h thanh toán phí dịch vụ
                </span>
              </div>
            </div>
            <div class="border-bottom-white pt-3 pb-3 d-flex ">
              <div class="mr-3 m-auto-0 pr-3 benefit-icon">
                <img src="<?php echo base_url() ?>assets/images/img/icon_saving.png" alt="">
              </div>
              <div>
                <span class="d-block font-weight-bold">
                  Tiết kiệm chi phí đến 40%
                </span>
                <div>
                  <ul>
                    <li>
                      Chỉ 3,000,000/100 mã 
                    </li>
                    <li>Báo giá công khai minh bạch</li>
                  </ul>
                </div>
              </div>
            </div>
            <div class="border-bottom-white pt-3 pb-3 d-flex ">
              <div class="mr-3 m-auto-0 pr-3 benefit-icon">
                <img src="<?php echo base_url() ?>assets/images/img/icon_barcode.png" alt="">
              </div>
              <div>
                <span class="d-block font-weight-bold">
                  Đăng ký thành công mã số mã vạch cho sản phẩm
                </span>
                <span>
                  Doanh nghiệp dễ dàng chuẩn hóa quản lý sản phẩm khi đưa ra thị trường. Có mã số mã vạch là điều kiện
                  cơ
                  bản để đưa sản phẩm vào các chuỗi phân phối lớn hoặc xuất khẩu ra nước ngoài
                </span>
              </div>
            </div>
          </div>

        </div>
      </div>
    </div>

  </div>

  <!-- procedure -->
  <div class="procedure" id="service">
    <div class="container">
      <div class="procedure-main">
        <p class="font-size-40 text-center font-weight-bold">Quy trình triển khai dịch vụ <br>
          mã số mã vạch với iCheck</p>

        <!-- desktop -->
        <div class="procedure-step text-center mt-5 isShowDesktop">
          <div class="row">
            <div class="col-md-3">
              <h6>
                Bước 1
              </h6>
              <h6 class="font-weight-normal">Doanh nghiệp liên hệ với iCheck bằng cách đăng ký ở dưới hoặc <br>Hotline
                0935050093 </h6>
            </div>
            <div class="col-md-3">
              <div class="step-icon m-auto">
                <img src="<?php echo base_url() ?>assets/images/img/icon_note.png" alt="">
              </div>
              <div class="pt-2">
                <span class="d-block m-auto vertical-tiles"></span>
              </div>
            </div>
            <div class="col-md-3">
              <h6>
                Bước 3
              </h6>
              <h6 class="font-weight-normal">Doanh nghiệp ký hợp đồng sử dụng dịch vụ với iCheck và hoàn thiện thủ tục
              </h6>
            </div>
            <div class="col-md-3">
              <div class="step-icon m-auto">
                <img src="<?php echo base_url() ?>assets/images/img/icon_setting.png" alt="">
              </div>
              <div class="pt-2">
                <span class="d-block m-auto vertical-tiles"></span>
              </div>
            </div>
          </div>
          <div class="pt-2 pb-3 line-step">
            <img src="<?php echo base_url() ?>assets/images/img/subtract.png" alt="" class="w-100">
          </div>
          <div class="row">
            <div class="col-md-3">
              <div class="pb-2">
                <span class="d-block m-auto vertical-tiles"></span>
              </div>
              <div class="step-icon m-auto">
                <img src="<?php echo base_url() ?>assets/images/img/icon_phone.png" alt="">
              </div>
            </div>
            <div class="col-md-3">
              <h6 class="mt-4">
                Bước 2
              </h6>
              <h6 class="font-weight-normal">Doanh nghiệp cung cấp thông tin cần thiết
              </h6>
            </div>
            <div class="col-md-3">
              <div class="pb-2">
                <span class="d-block m-auto vertical-tiles"></span>
              </div>
              <div class="step-icon m-auto">
                <img src="<?php echo base_url() ?>assets/images/img/icon_note2.png" alt="">
              </div>
            </div>
            <div class="col-md-3">
              <h6 class="mt-4">
                Bước 4
              </h6>
              <h6 class="font-weight-normal">iCheck tiến hành đăng ký mã số mã vạch và bàn giao kết quả cho doanh nghiệp
              </h6>
            </div>
          </div>
        </div>

        <!-- mobile -->
        <div class="procedure-step mt-5 isShowMobile">
          <div class="row">
            <div class="col-sm-5 text-right">
              <div class="d-flex mt-5">
                <div class="step-icon m-auto">
                  <img src="<?php echo base_url() ?>assets/images/img/icon_phone.png" alt="">
                </div>
                <div class="pt-2">
                  <span class="d-block m-auto vertical-tiles"></span>
                </div>
              </div>
              <div class="mt-5">
                <h6 class="pt-4">
                  Bước 2
                </h6>
                <h6 class="font-weight-normal">Doanh nghiệp cung cấp thông tin cần thiết
                </h6>
              </div>
              <div class="d-flex pt-5 mt-4">
                <div class="step-icon m-auto">
                  <img src="<?php echo base_url() ?>assets/images/img/icon_note2.png" alt="">
                </div>
                <div class="pt-2">
                  <span class="d-block m-auto vertical-tiles"></span>
                </div>
              </div>
              <div class="mt-5">
                <h6 class="mt-4">
                  Bước 4
                </h6>
                <h6 class="font-weight-normal">iCheck tiến hành đăng ký mã số mã vạch và bàn giao kết quả cho doanh
                  nghiệp
                </h6>
              </div>
            </div>
            <div class="col-sm-2">
              <div class="pt-2 pb-3 line-step text-center">
                <img src="<?php echo base_url() ?>assets/images/img/verticalStep.png" alt="">
              </div>
            </div>
            <div class="col-sm-5 text-left">
              <div class="mt-5">
                <h6 class="mt-4">
                  Bước 2
                </h6>
                <h6 class="font-weight-normal">Doanh nghiệp cung cấp thông tin cần thiết
                </h6>
              </div>
              <div class="d-flex mt-5">
                <div class="pt-4">
                  <span class="d-block m-auto vertical-tiles"></span>
                </div>
                <div class="step-icon m-auto">
                  <img src="<?php echo base_url() ?>assets/images/img/icon_note.png" alt="">
                </div>
              </div>
              <div class="mt-5">
                <h6 class="pt-4">
                  Bước 3
                </h6>
                <h6 class="font-weight-normal">Doanh nghiệp ký hợp đồng sử dụng dịch vụ với iCheck và hoàn thiện thủ tục
                </h6>
              </div>
              <div class="d-flex mt-5 pt-3">
                <div class="pt-4">
                  <span class="d-block m-auto vertical-tiles"></span>
                </div>
                <div class="step-icon m-auto">
                  <img src="<?php echo base_url() ?>assets/images/img/icon_note.png" alt="">
                </div>
              </div>
            </div>
          </div>
        </div>

        <div class="mt-5">
          <div class="row">
            <div class="col-md-3 col-6">
              <div
                class="procedure-benifit_step d-flex justify-content-center align-items-center border-radius-20 mt-2">
                <div class="mr-1 m-auto-0 pr-1">
                  <img src="<?php echo base_url() ?>assets/images/img/icon_time2.png" alt="" class="w-100">
                </div>
                <div>
                  <span class="d-block font-weight-bold font-size-18">
                    Tiết kiệm 90% thời gian
                  </span>
                </div>
              </div>
            </div>
            <div class="col-md-3 col-6">
              <div
                class="procedure-benifit_step d-flex justify-content-center align-items-center border-radius-20 mt-2">
                <div class="mr-1 m-auto-0 pr-1">
                  <img src="<?php echo base_url() ?>assets/images/img/icon_service.png" alt="" class="w-100">
                </div>
                <div>
                  <span class="d-block font-weight-bold font-size-18">
                    Vượt 80% mong đợi khi sử dụng dịch vụ
                  </span>
                </div>
              </div>
            </div>
            <div class="col-md-3 col-6">
              <div
                class="procedure-benifit_step d-flex justify-content-center align-items-center border-radius-20 mt-2">
                <div class="mr-1 m-auto-0 pr-1">
                  <img src="<?php echo base_url() ?>assets/images/img/icon_time2.png" alt="" class="w-100">
                </div>
                <div>
                  <span class="d-block font-weight-bold font-size-18">
                    Tiết kiệm 90% thời gian
                  </span>
                </div>
              </div>
            </div>
            <div class="col-md-3 col-6">
              <div
                class="procedure-benifit_step d-flex justify-content-center align-items-center border-radius-20 mt-2">
                <div class="mr-1 m-auto-0 pr-1">
                  <img src="<?php echo base_url() ?>assets/images/img/icon_hand.png" alt="" class="w-100">
                </div>
                <div>
                  <span class="d-block font-weight-bold font-size-18">
                    Hơn 20,000 doanh nghiệp đã hợp tác
                  </span>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div class="text-center mt-5">
         <a href="#promo"> <button type="button" class="btn  text-uppercase  btn-warning btn-orange font-weight-bold btn-lg">ĐĂNG KÝ
            NGAY</button></a>
        </div>

      </div>
    </div>
  </div>

  <!-- Reason -->
  <div class="reason" id="use">
    <div class="container">
      <div class="procedure-main">
        <p class="font-size-40 text-center font-weight-bold">Tại sao nên đăng ký <br> mã số mã vạch với iCheck</p>
        <div class="procedure-main_content mt-5">
          <div class="row">
            <div class="col-md-6 col-12">
              <div>
                <img src="<?php echo base_url() ?>assets/images/img/background_reason.png" alt="" class="w-100">
              </div>
            </div>
            <div class="col-md-6 col-12">
              <div class="pt-3 pb-3 d-flex ">
                <div class="mr-3 m-auto-0 pr-3 benefit-icon">
                  <img src="<?php echo base_url() ?>assets/images/img/icon_note3.png" alt="">
                </div>
                <div class="border-bottom-grey pb-3">
                  <span class="d-block font-weight-bold">
                    Phân định sản phẩm chính xác tuyệt đối
                  </span>
                  <span>
                    iCheck hỗ trợ khách hàng phân định mã số mã vạch một cách chính xác và tuyệt đối
                  </span>
                </div>
              </div>
              <div class="pt-3 pb-3 d-flex ">
                <div class="mr-3 m-auto-0 pr-3 benefit-icon">
                  <img src="<?php echo base_url() ?>assets/images/img/icon_start.png" alt="">
                </div>
                <div class="border-bottom-grey pb-3">
                  <span class="d-block font-weight-bold">
                    7 năm kinh nghiệm
                  </span>
                  <span>
                    iCheck đang là đối tác chính thức của GS1 Việt Nam – Trung tâm mã số mã vạch quốc gia
                  </span>
                </div>
              </div>
              <div class="pt-3 pb-3 d-flex ">
                <div class="mr-3 m-auto-0 pr-3 benefit-icon">
                  <img src="<?php echo base_url() ?>assets/images/img/icon_time3.png" alt="">
                </div>
                <div class="border-bottom-grey pb-3">
                  <span class="d-block font-weight-bold">
                    Thời gian nhận mã nhanh chóng
                  </span>
                  <span>
                    Chỉ sau 1 ngày nộp hồ sơ, doanh nghiệp sẽ nhận được mã số mã vạch tạm thời
                  </span>
                </div>
              </div>

              <div class="pt-3 pb-3 d-flex ">
                <div class="mr-3 m-auto-0 pr-3 benefit-icon">
                  <img src="<?php echo base_url() ?>assets/images/img/icon_headphone.png" alt="">
                </div>
                <div class="border-bottom-grey pb-3">
                  <span class="d-block font-weight-bold">
                    Tư vấn tận tình 24/7
                  </span>
                  <span>
                    Đội ngũ nhân viên vững kiến thức về pháp lý, chuyên phân định mã số, mã vạch cho DN
                  </span>
                </div>
              </div>

              <div class="pt-3 pb-3 d-flex ">
                <div class="mr-3 m-auto-0 pr-3 benefit-icon">
                  <img src="<?php echo base_url() ?>assets/images/img/icon_note3.png" alt="">
                </div>
                <div class="border-bottom-grey pb-3">
                  <span class="d-block font-weight-bold">
                    Chi phí thấp nhất thị trường
                  </span>
                  <span>
                    Hợp tác với iCheck doanh nghiêp tiết kiệm chi phí đến 40% so với các nhà cung cấp khác
                  </span>
                </div>
              </div>

              <div class="pt-3 pb-3 d-flex ">
                <div class="mr-3 m-auto-0 pr-3 benefit-icon">
                  <img src="<?php echo base_url() ?>assets/images/img/icon_setting2.png" alt="">
                </div>
                <div class=" pb-3">
                  <span class="d-block font-weight-bold">
                    Quy trình triển khai nhanh chóng
                  </span>
                  <span>
                    Doanh nghiệp chỉ cần cung cấp đủ giấy tờ, iCheck hỗ trợ chuẩn bị đầy đủ thông tin một cách chính
                    xác,
                    giảm
                    thiểu rủi ro phát sinh
                  </span>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- Client -->
  <div class="client">
    <div class="container">
      <div class="client-main">
        <p class="font-size-40 text-center font-weight-bold color-green">Khách hàng của chúng tôi</p>
        <span class="text-center d-block">Hàng trăm doanh nghiệp đã chọn chúng tôi. Vậy bạn còn chần chừ gì nữa!</span>
        <div class="client-category mt-5">
          <div class="d-flex d-flex">
            <div class="font-weight-bold category-name ">
              Mỹ phẩm
            </div>
            <div class="font-weight-bold client-logo d-flex flex-wrap">
              <div
                class="border-radius-8 background-white client-logo_item d-flex justify-content-center align-items-center p-2 mr-2 ml-2 mt-2">
                <img src="<?php echo base_url() ?>assets/images/img/logo_sakura.png" alt="">
              </div>
              <div
                class="border-radius-8 background-white client-logo_item d-flex justify-content-center align-items-center p-2 mr-2 ml-2 mt-2">
                <img src="<?php echo base_url() ?>assets/images/img/logo mypham salozon 1.png" alt="">
              </div>
              <div
                class="border-radius-8 background-white client-logo_item d-flex justify-content-center align-items-center p-2 mr-2 ml-2 mt-2">
                <img src="<?php echo base_url() ?>assets/images/img/logo cty saothaiduong.png" alt="">
              </div>
              <div
                class="border-radius-8 background-white client-logo_item d-flex justify-content-center align-items-center p-2 mr-2 ml-2 mt-2">
                <img src="<?php echo base_url() ?>assets/images/img/logo amway.png" alt="">
              </div>
              <div
                class="border-radius-8 background-white client-logo_item d-flex justify-content-center align-items-center p-2 mr-2 ml-2 mt-2">
                <img src="<?php echo base_url() ?>assets/images/img/logo benhvien huunghi hanoi.png" alt="">
              </div>
            </div>
          </div>
          <div class="d-flex d-flex">
            <div class="font-weight-bold category-name">
              Dược phẩm
            </div>
            <div class="font-weight-bold client-logo d-flex flex-wrap">
              <div
                class="border-radius-8 background-white client-logo_item d-flex justify-content-center align-items-center p-2 mr-2 ml-2 mt-2">
                <img src="<?php echo base_url() ?>assets/images/img/logo traphaco 1.png" alt="">
              </div>
              <div
                class="border-radius-8 background-white client-logo_item d-flex justify-content-center align-items-center p-2 mr-2 ml-2 mt-2">
                <img src="<?php echo base_url() ?>assets/images/img/logo nam duoc 1.png" alt="">
              </div>
              <div
                class="border-radius-8 background-white client-logo_item d-flex justify-content-center align-items-center p-2 mr-2 ml-2 mt-2">
                <img src="<?php echo base_url() ?>assets/images/img/logo cong ty duoc pham binh minh 1.png" alt="">
              </div>
              <div
                class="border-radius-8 background-white client-logo_item d-flex justify-content-center align-items-center p-2 mr-2 ml-2 mt-2">
                <img src="<?php echo base_url() ?>assets/images/img/logo_duoc_thanhtrang 1.png" alt="">
              </div>
              <div
                class="border-radius-8 background-white client-logo_item d-flex justify-content-center align-items-center p-2 mr-2 ml-2 mt-2">
                <img src="<?php echo base_url() ?>assets/images/img/logo visgerpharm 1.png" alt="">
              </div>
            </div>
          </div>
          <div class="d-flex d-flexmt-3">
            <div class="font-weight-bold category-name">
              Tiêu dùng
            </div>
            <div class="font-weight-bold client-logo d-flex flex-wrap">
              <div
                class="border-radius-8 background-white client-logo_item d-flex justify-content-center align-items-center p-2 mr-2 ml-2 mt-2">
                <img src="<?php echo base_url() ?>assets/images/img/logo thinhlong 1.png" alt="">
              </div>
              <div
                class="border-radius-8 background-white client-logo_item d-flex justify-content-center align-items-center p-2 mr-2 ml-2 mt-2">
                <img src="<?php echo base_url() ?>assets/images/img/logo gom chudau 1.png" alt="">
              </div>
              <div
                class="border-radius-8 background-white client-logo_item d-flex justify-content-center align-items-center p-2 mr-2 ml-2 mt-2">
                <img src="<?php echo base_url() ?>assets/images/img/logo hadina 1.png" alt="">
              </div>
              <div
                class="border-radius-8 background-white client-logo_item d-flex justify-content-center align-items-center p-2 mr-2 ml-2 mt-2">
                <img src="<?php echo base_url() ?>assets/images/img/logo botgiat lix 1.png" alt="">
              </div>
              <div
                class="border-radius-8 background-white client-logo_item d-flex justify-content-center align-items-center p-2 mr-2 ml-2 mt-2">
                <img src="<?php echo base_url() ?>assets/images/img/logo haeva 1.png" alt="">
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- rating -->
  <div class="rating" id="sale">
    <div class="container">
      <div class="rating-main">
        <p class="font-size-40 text-center font-weight-bold color-green">iCheck - Đơn vị hàng đầu về dịch vụ <br>cung
          cấp
          mã số mã vạch</p>
        <span class="text-center d-block w-50 m-auto">iCheck tự hào là đơn vị được đầu tư & tham gia tập đoàn VNLife từ
          12/2019.
          Chúng
          tôi đi đầu trong việc cung cấp các dịch vụ pháp lý liên quan đến sản phẩm của Doanh nghiệp, hỗ trợ doanh
          nghiệp
          từ những bước đi đầu tiên đến khi đưa sản phẩm tới thị trường.
        </span>
        <p class="font-size-30 text-center font-weight-bold color-green mt-4">
          Sở hữu ứng dụng iCheck Scanner & Barcode Việt
        </p>
        <div class="mt-5">
          <div class="row">
            <div class="col-md-3 col-6">
              <div
                class="rating-item d-flex flex-wrap justify-content-center align-items-center border-radius-20 background-orange p-3 mt-2">
                <div class="mr-1 pr-1 mb-3">
                  <img src="<?php echo base_url() ?>assets/images/img/icon_download.png" alt="">
                </div>
                <div class="w-100 text-center">
                  <span class="d-block font-weight-bold font-size-18">
                    20 triệu+
                  </span>
                  <span>lượt tải</span>
                </div>
              </div>
            </div>
            <div class="col-md-3 col-6">
              <div
                class="rating-item d-flex flex-wrap justify-content-center align-items-center border-radius-20 background-orange p-3 mt-2">
                <div class="mr-1 pr-1 mb-3">
                  <img src="<?php echo base_url() ?>assets/images/img/icon_box.png" alt="">
                </div>
                <div class="w-100 text-center">
                  <span class="d-block font-weight-bold font-size-18">
                    6 triệu+
                  </span>
                  <span>SKUs sản phẩm</span>
                </div>
              </div>
            </div>
            <div class="col-md-3 col-6">
              <div
                class="rating-item d-flex flex-wrap justify-content-center align-items-center border-radius-20 background-orange p-3 mt-2">
                <div class="mr-1 pr-1 mb-3">
                  <img src="<?php echo base_url() ?>assets/images/img/icon_scan.png" alt="">
                </div>
                <div class="w-100 text-center">
                  <span class="d-block font-weight-bold font-size-18">
                    Hơn 20,000 doanh nghiệp đã hợp tác
                  </span>
                  <span>
                    500 triệu+
                  </span>
                </div>
              </div>
            </div>
            <div class="col-md-3 col-6">
              <div
                class="rating-item d-flex flex-wrap justify-content-center align-items-center border-radius-20 background-orange p-3 mt-2">
                <div class="mr-1 pr-1 mb-3">
                  <img src="<?php echo base_url() ?>assets/images/img/icon_user.png" alt="">
                </div>
                <div class="w-100 text-center">
                  <span class="d-block font-weight-bold font-size-18">
                    2 triệu+
                  </span>
                  <span>lượt người dùng thường xuyên mỗi tháng</span>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- review -->
  <div class="review">
    <div class="container">
      <div class="review-main">
        <div class="row">
          <div class="col-md-6 col-12 pt-3">
            <div class="review-newspapers ">
              <span class="font-size-30 font-weight-bold">Báo chí nói về iCheck</span>
              <a href="https://icheck.com.vn/tang-1-trieu-khau-trang-phong-tranh-dich-covid-19/" class="text-dark" target="blank"><div class="review-newspapers_content d-flex align-items-center mt-4">
                <div class="img-thumnai border-radius-8">
                  <img src="<?php echo base_url() ?>assets/images/img/img_thumnai1.png" alt="" class="w-100 h-100">
                </div>
                <div class="description ml-3">
                  <h6 class="color-green font-size-18">Tặng 1 triệu khẩu trang phòng tránh dịch COVID-19</h6>
                  <span class="date font-size-14 font-italic">23/03/2020</span>
                </div>
              </div></a>
              <a href="https://icheck.com.vn/dua-nong-san-len-san-thuong-mai-dien-tu-huong-di-moi-cho-nong-san-viet/" class="text-dark" target="blank">
              <div class="review-newspapers_content d-flex align-items-center mt-3">
                <div class="img-thumnai border-radius-8 mr-3">
                  <img src="<?php echo base_url() ?>assets/images/img/img_thumnai2.png" alt="" class="w-100 h-100">
                </div>
                <div class="description">
                  <h6 class="font-size-18">Đưa nông sản lên sàn thương mại điện tử, hướng đi mới cho nông sản Việt</h6>
                  <span class="date font-size-14 font-italic">23/03/2020</span>
                </div>
              </div>
          </a>
          <a href="https://icheck.com.vn/icheck-to-chuc-su-kien-tang-doanh-so-ban-hang-hau-covid-19/" class="text-dark" target="blank">
              <div class="review-newspapers_content d-flex align-items-center mt-3">
                <div class="img-thumnai border-radius-8 mr-3">
                  <img src="<?php echo base_url() ?>assets/images/img/img_thumnai3.png" alt="" class="w-100">
                </div>
                <div class="description">
                  <h6 class="font-size-18">iCheck tổ chức sự kiện Tăng doanh số bán hàng hậu Covid 19</h6>
                  <span class="date font-size-14 font-italic">23/03/2020</span>
                </div>
              </div>
          </a>
            </div>
          </div>

          <div class="col-md-6 col-12 pt-3">
            <div class="review-client">
              <span class="font-size-30 font-weight-bold d-block">Phản hồi của khách hàng</span>

              <div id="review-comment" class="carousel slide" data-ride="carousel">
                <ol class="carousel-indicators pt-3">
                  <li data-target="#review-comment" data-slide-to="0" class="active"></li>
                  <li data-target="#review-comment" data-slide-to="1"></li>
                  <li data-target="#review-comment" data-slide-to="2"></li>
                </ol>
                <div class="carousel-inner">
                  <div class="carousel-item active">
                    <div class="row">
                      <div class="col-md-6">
                        <div class="review-client_content p-3 mt-4">
                          <div>
                            <img src="<?php echo base_url() ?>assets/images/img/logo_thie_long.png" alt="">
                          </div>
                          <p class="mt-3">“Công ty tôi đã nhanh chóng có mã vạch nhờ đội ngũ giàu kinh nghiệm của
                            iCheck”</p>
                          <div class="info-member-review">
                            <span class="font-weight-bold">Bà Lan</span>
                            <span class="d-block">Chủ doanh nghiệp THIÊN LONG</span>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="review-client_content p-3 mt-4">
                          <div>
                            <img src="<?php echo base_url() ?>assets/images/img/logo_dura.png" alt="">
                          </div>
                          <p class="mt-3">“Đội ngũ iCheck rất nhiệt tình và giàu kinh nghiệm, đã tư vấn cho tôi đăng ký
                            rất kỹ càng”
                          </p>
                          <div class="info-member-review">
                            <span class="font-weight-bold">Ông Tú</span>
                            <span class="d-block">Giám đốc kinh doanh SƠN DURA</span>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="carousel-item">
                    <div class="row">
                      <div class="col-md-6">
                        <div class="review-client_content p-3 mt-4">
                          <div>
                            <img src="<?php echo base_url() ?>assets/images/img/logo_dura.png" alt="">
                          </div>
                          <p class="mt-3">“Đội ngũ iCheck rất nhiệt tình và giàu kinh nghiệm, đã tư vấn cho tôi đăng ký
                            rất kỹ càng”
                          </p>
                          <div class="info-member-review">
                            <span class="font-weight-bold">Ông Tú</span>
                            <span class="d-block">Giám đốc kinh doanh SƠN DURA</span>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="review-client_content p-3 mt-4">
                          <div>
                            <img src="<?php echo base_url() ?>assets/images/img/logo_thienan.png" alt="">
                          </div>
                          <p class="mt-3">“Với iCheck rất nhanh chóng, chỉ trong vòng 1 ngày tôi đã nhận được kết quả
                            hoàn thiện”
                          </p>
                          <div class="info-member-review">
                            <span class="font-weight-bold">Ông Lâm</span>
                            <span class="d-block">Chủ doanh nghiệp Mỹ phẩm Thiên An</span>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="carousel-item">
                    <div class="row">
                        <div class="col-md-6">
                        <div class="review-client_content p-3 mt-4">
                          <div>
                            <img src="<?php echo base_url() ?>assets/images/img/logo_thienan.png" alt="" >
                          </div>
                          <p class="mt-3">“Với iCheck rất nhanh chóng, chỉ trong vòng 1 ngày tôi đã nhận được kết quả
                            hoàn thiện”
                          </p>
                          <div class="info-member-review">
                            <span class="font-weight-bold">Ông Lâm</span>
                            <span class="d-block">Chủ doanh nghiệp Mỹ phẩm Thiên An</span>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="review-client_content p-3 mt-4">
                          <div>
                            <img src="<?php echo base_url() ?>assets/images/img/logo_thie_long.png" alt="">
                          </div>
                          <p class="mt-3">“Công ty tôi đã nhanh chóng có mã vạch nhờ đội ngũ giàu kinh nghiệm của
                            iCheck”</p>
                          <div class="info-member-review">
                            <span class="font-weight-bold">Bà Lan</span>
                            <span class="d-block">Chủ doanh nghiệp THIÊN LONG</span>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>

                </div>
              </div>
              <!-- <div class="arrow-slide">
                <a class="carousel-control-prev" href="#review-comment" role="button" data-slide="prev">
                  <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                  <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#review-comment" role="button" data-slide="next">
                  <span class="carousel-control-next-icon" aria-hidden="true"></span>
                  <span class="sr-only">Next</span>
                </a>
              </div> -->

              <!-- <div class="review-client_content p-3 mt-4">
                              <div>
                                <img src="<?php echo base_url() ?>assets/images/img/logo_thie_long.png" alt="">
                              </div>
                              <p>“Công ty tôi đã nhanh chóng có mã vạch nhờ đội ngũ giàu kinh nghiệm của iCheck”</p>
                              <div class="info-member-review">
                                <span class="font-weight-bold">Bà Lan</span>
                                <span class="d-block">Chủ doanh nghiệp THIÊN LONG</span>
                              </div>
                            </div> -->
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- partner -->
  <div class="partner">
    <div class="container">
      <div class="partner-main">
        <div class="d-flex align-items-center">
          <div class="color-green font-size-18 font-weight-bold">
            Đối tác của iCheck
          </div>
          <div class="d-flex flex-wrap">
            <div
              class="border-radius-8 background-white client-logo_item d-flex justify-content-center align-items-center p-2 mr-2 ml-2 mt-2">
              <img src="<?php echo base_url() ?>assets/images/img/logo_soibien.png" alt="">
            </div>
            <div
              class="border-radius-8 background-white client-logo_item d-flex justify-content-center align-items-center p-2 mr-2 ml-2  mt-2">
              <img src="<?php echo base_url() ?>assets/images/img/logo_phongvu.png" alt="">
            </div>
            <div
              class="border-radius-8 background-white client-logo_item d-flex justify-content-center align-items-center p-2 mr-2 ml-2  mt-2">
              <img src="<?php echo base_url() ?>assets/images/img/logo_vnpay.png" alt="">
            </div>
            <div
              class="border-radius-8 background-white client-logo_item d-flex justify-content-center align-items-center p-2 mr-2 ml-2  mt-2">
              <img src="<?php echo base_url() ?>assets/images/img/logo_viettell.png" alt="">
            </div>
            <div
              class="border-radius-8 background-white client-logo_item d-flex justify-content-center align-items-center p-2 mr-2 ml-2  mt-2">
              <img src="<?php echo base_url() ?>assets/images/img/logo_bnc.png" alt="">
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- promo -->
  <div class="promo" id="promo">
    <div class="container">
      <div class="promo-main">
        <div class="row">
          <div class="col-md-6 col-12">
            <p class="color-yellow font-size-30 font-weight-bold">Ưu đãi đặc biệt khi hợp tác với iCheck</p>
            <div class="d-flex align-items-center text-white">
              <div class="border-bottom-white pt-3 pb-3 d-flex ">
                <div class="mr-3 m-auto-0 pr-3 promo-icon">
                  <img src="<?php echo base_url() ?>assets/images/img/icon_free.png" alt="">
                </div>
                <div>
                  <span class="d-block font-weight-bold">
                    MIỄN PHÍ kiểm nghiệm sản phẩm
                  </span>
                  <ul>
                    <li>
                      Doanh nghiệp không mất thêm bất kỳ khoản phí nào
                    </li>
                    <li>Tư vấn miễn phí giải pháp phù hợp nhất để lên chỉ tiêu kiểm nghiệm</li>
                  </ul>
                </div>
              </div>
            </div>
            <div class="d-flex align-items-center text-white">
              <div class="border-bottom-white pt-3 pb-3 d-flex ">
                <div class="mr-3 m-auto-0 pr-3 promo-icon">
                  <img src="<?php echo base_url() ?>assets/images/img/icon_headphone2.png" alt="">
                </div>
                <div>
                  <span class="d-block font-weight-bold">
                    Tổng đài tư vấn miễn phí các vấn đề pháp lý liên quan đến sản phẩm
                  </span>
                  <span class="d-block">
                    Đây là kênh hỗ trợ tư vấn và giải đáp các vấn đề pháp lý có liên quan đến sản phẩm bởi đội ngũ tư
                    vấn viên của iCheck.
                  </span>
                </div>
              </div>
            </div>
            <div class="d-flex align-items-center text-white">
              <div class="pt-3 pb-3 d-flex ">
                <div class="mr-3 m-auto-0 pr-3 promo-icon">
                  <img src="<?php echo base_url() ?>assets/images/img/icon_setting3.png" alt="">
                </div>
                <div>
                  <span class=" d-block font-weight-bold">
                    Tích hợp combo 3 trong 1
                  </span>
                  <span class="d-block">Khi sử dụng dịch vụ của iCheck, bạn sẽ được tích hợp combo 3 dịch vụ từ MSMV: QR
                    marketing, Minh bạch thông tin, Hoá đơn điện tử</span>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-6 col-12 promo-registration">
            <div class="background-white registration p-5">
              <h6 class="font-size-30 color-green">Đăng ký mã số mã vạch</h6>
              <ul class="detail">
                <li>Tiết kiệm chi phí và thời gian</li>
                <li>Lấy mã chỉ sau 1 ngày</li>
                <li>Miễn phí kiểm nghiệm sản phẩm</li>
              </ul>
              <?php
                            $current_url = current_url();
                            if($_SERVER['QUERY_STRING']){
                                $current_url .= '?' . $_SERVER['QUERY_STRING'];
                            }
                        ?>
                        <form action="<?php echo $current_url ?>" method="post" class="wpcf7-form init" novalidate="novalidate" data-status="init">
                            <div style="display: none;">
                                <input type="hidden" name="_wpcf7" value="3157">
                                <input type="hidden" name="_wpcf7_version" value="5.3.2">
                                <input type="hidden" name="_wpcf7_locale" value="en_US">
                                <input type="hidden" name="_wpcf7_unit_tag" value="wpcf7-f3157-o1">
                                <input type="hidden" name="_wpcf7_container_post" value="0">
                                <input type="hidden" name="_wpcf7_posted_data_hash" value="">
                            </div>
                <div class="form-group">
                  <input type="text" class="form-control mt-4" name="full_name" id="exampleFormControlInput1" form-control-lg
                    placeholder="Họ tên">
                  <input type="text" class="form-control mt-4" name="sodienthoai" id="exampleFormControlInput1" form-control-lg
                    placeholder="Số điện thoại">
                  <input type="email" class="form-control mt-4" name="email" id="exampleFormControlInput1" form-control-lg
                    placeholder="Email">
                  <input type="text" class="form-control mt-4" name="company" id="exampleFormControlInput1" form-control-lg
                    placeholder="Tên Doanh nghiệp">
                </div>
                <div class="text-center mt-5">
                    <input type="text" name="get_otp" class="d-none">
                  <button type="submit" class="btn text-uppercase btn-warning btn-orange font-weight-bold btn-lg">Đăng
                    ký
                    ngay</button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="qd-landingpage-wraper">
    <div class="footer">
        <div class="container">
            <div class="row">
                <div class="col-12 col-lg-5 footer-left-wrapper">
                    <img src="https://icheck.com.vn/wp-content/uploads/2021/05/logo.svg" class="logo">
                    <p>Công ty cổ phần iCheck</p>
                    <div class="get-app row">
                        <div class="get-app-qr-code">
                            <img src="https://icheck.com.vn/wp-content/themes/icheck/assets/images/footer_qr.png"  class="qrcode"/>
                        </div>
                        <div class="get-app-store">
                            <a href="https://apps.apple.com/app/id1001036590" data-wpel-link="external" rel="nofollow external noopener noreferrer"><noscript><img src="https://icheck.com.vn/wp-content/themes/icheck/assets/images/app_store.svg" alt="" loading="lazy"></noscript><img class=" lazyloaded" src="https://icheck.com.vn/wp-content/themes/icheck/assets/images/app_store.svg" data-src="https://icheck.com.vn/wp-content/themes/icheck/assets/images/app_store.svg" alt="" loading="lazy"></a>
                            <a href="https://play.google.com/store/apps/details?id=vn.icheck.android&amp;hl=vi" data-wpel-link="external" rel="nofollow external noopener noreferrer"><noscript><img src="https://icheck.com.vn/wp-content/themes/icheck/assets/images/google_play.svg" alt="" loading="lazy"></noscript><img class=" lazyloaded" src="https://icheck.com.vn/wp-content/themes/icheck/assets/images/google_play.svg" data-src="https://icheck.com.vn/wp-content/themes/icheck/assets/images/google_play.svg" alt="" loading="lazy"></a>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-lg-7 footer-right-wrapper" >
                    <ul class="address">
                        <li>
                            <p class="office">Văn phòng miền Bắc</p>
                            <p>Tầng 12 Tòa nhà Diamond Flower, số 48 Lê Văn Lương, Khu đô thị mới N1, phường Nhân Chính, quận Thanh Xuân, Hà Nội</p>
                        </li>
                        <li>
                            <p class="office">Văn phòng Hồ Chí Minh</p>
                            <p>Tầng 7 Toà nhà Hoàng Anh Gia Lai Safomec, số 7/1 Thành Thái phường 14 quận 10, Tp Hồ Chí Minh</p>
                        </li>
                    </ul>
                    <div class="contact">
                        <span>
                            <i class="fa fa-phone" aria-hidden="true"></i>0935050093
                        </span>
                        <span>
                            <i class="fa fa-envelope" aria-hidden="true"></i>trungld@icheck.com.vn
                        </span>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
<?php $otp_step = $this->session->flashdata('otp_step'); ?>
<?php if($otp_step): ?>
    <div class="modal fade" id="otp_confirm">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header" style="background-color: #238A3E;">
                    <h4 class="modal-title" style="color: #fff;">Xác nhận đăng ký</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button> 
                </div>
                
                <div class="modal-body">
                    <div class="row">
                        <p style="width: 80%;text-align: center;margin: 0 auto;margin-bottom: 20px;">
                            OTP đã được gửi tới số điện thoại của bạn. Nhập OTP để xác nhận đăng ký
                        </p>
                        <form class="form-inline" style="width: 100%;" method="post" action="<?php echo base_url(uri_string()) ?>">
                        <div style="margin: 0 auto;margin-bottom: 20px; width: 100%;text-align: center;">
                            <input class="form-control" name="otp" placeholder="Nhập OTP" style="margin-right: 15px;max-width: 60%;margin: 0 auto;">
                            <button type="submit" name="confirm_otp" class="btn btn-default" style="background-color: #238A3E; color: #fff;">Xác nhận</button> 
                        </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php endif; ?>
    <script src="<?php echo base_url('assets/js/owl_carousel/owl.carousel.min.js?v=' . VERSION_WEB) ?>"></script> 
    <script src="<?php echo base_url('assets/js/bootstrap.min.js?v=' . VERSION_WEB) ?>" id="script-resource-3"></script> 
    <script src="<?php echo base_url('assets/js/bootstrap-notify.min.js?v=' . VERSION_WEB) ?>"></script>
    <?php $error = $this->session->flashdata('error');
        if($error){
            $message = $error;
        }
        $success = $this->session->flashdata('success');
        if($success){
            $message = $success;
        }
        if(isset($message)){
            $message = str_replace("\n", '', $message);
        }
        
    ?>
    <script>
    $(document).ready(function(){
        <?php if($otp_step): ?>
        $('#otp_confirm').modal({backdrop: 'static', keyboard: false});
        <?php endif; ?>
        //$('.cf7-frm-wrapper .wpcf7-submit').on('click',function(event){
            event.preventDefault();
            <?php if($message):?>
            $.notify({
            icon: 'glyphicon glyphicon-bell',
            message: <?php echo "'" . $message . "',"; ?>
            title: 'Thông báo',
        },{
            type:  <?php echo ($error)?"'danger'":"'success'" ?>,
            delay: 5000,
        });
        <?php endif;?>
            //var target = $(event.target);
            //var data = {
            //    "phoneNumber": $('.wpcf7-form-control-wrap input[name="sodienthoai"]').val(),
            //    "password": $('.wpcf7-form-control-wrap input[name="password"]').val(),
            //    "confirmPassword": $('.wpcf7-form-control-wrap input[name="check-password"]').val(),
            //    "firstName": $('.wpcf7-form-control-wrap input[name="company"]').val(),
            //    "lastName": $('.wpcf7-form-control-wrap input[name="company"]').val(),
            //    "tax": $('.wpcf7-form-control-wrap input[name="tax"]').val()};
            //console.log(data);
            //
            //$.ajax({
            //    type: "post",
            //    dataType: "json",
            //    contentType: 'application/json',
            //    url: "https://qrmkt-api.dev.icheck.vn/qr-code-free/api/v1/landing-page/register/request",
            //    data:JSON.stringify(data),
            //    success: function (response) {
            //        if (response.code == 1) {
            //            $('.cf7-frm-wrapper form').submit();
            //            setTimeout(function(){ window.location.replace("<?php echo site_url('/success/') ?>"); }, 500);
            //        } else {
            //            alert(response.message)
            //        }
            //    },
            //})
        //})
        
        <?php if($error && !isset($otp_step)): ?>
            $([document.documentElement, document.body]).animate({
                scrollTop: $("#form-register").offset().top
            }, 1500);
        <?php endif; ?>
    })
    </script>
    
    
    <!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-WFK9B3D');</script>
<!-- End Google Tag Manager -->

<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-WFK9B3D"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
</body>
</html>


