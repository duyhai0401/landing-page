<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title><?php echo WEB_TITLE ?></title>
	<link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url('assets/images/icon-title.jpg') ?>" />
    
	<link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap.min.css?v=' . VERSION_WEB)    ?>" id="style-resource-4">
    <link rel="stylesheet" href="<?php echo base_url('assets/css/landingpage.css?v=' . VERSION_WEB) ?>" id="style-resource-8">
	<link rel="stylesheet" href="<?php echo base_url('assets/js/owl_carousel/owl.carousel.min.css?v=' . VERSION_WEB) ?>" id="style-resource-6">
	
	<link rel="stylesheet" href="<?php echo base_url('assets/font-awesome-4.7.0/css/font-awesome.css?v=' . VERSION_WEB) ?>" id="style-resource-5">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/animate.min.css?v=' . VERSION_WEB) ?>">
	
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@400;500;700;900&display=swap" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
</head>
<body>
<div class="qd-landingpage-wraper">
    <div class="section-1">
        <div class="container">
            <div class="row">
                <div class="col">
                    <nav class="navbar navbar-expand-lg navbar-light">
                        <a class="navbar-brand" href=""><img src="<?php echo base_url() ?>assets/images/new_landing_page/logo.png"></a>
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-icon"><i class="fa fa-bars" aria-hidden="true"></i></span>
                        </button>
                        <div class="collapse navbar-collapse" id="navbarNav">
                            <ul class="navbar-nav ml-auto">
								<li class="nav-item">
                                    <a class="nav-link" href="<?php echo base_url() ?>">Tính năng</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="<?php echo base_url() ?>">Hướng dẫn</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="https://marketing.icheck.vn">Đăng nhập</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="<?php echo base_url() ?>">Trải nghiệm miễn phí</a>
                                </li>
                            </ul>
                        </div>
                    </nav></div>

                <div class="w-100"></div>
                <div class="banner-success col-xl-12 col-lg-12 text-center">
                    
                </div>
            </div>
        </div>
    </div>
	
    <div class="footer">
        <div class="container">
            <div class="row">
                <div class="col-12 col-lg-5 footer-left-wrapper" >
                    <img src="https://icheck.com.vn/wp-content/uploads/2021/05/logo.svg" class="logo">
                    <p>Công ty cổ phần iCheck</p>
                    <div class="get-app row">
                        <div class="get-app-qr-code">
                            <img src="https://icheck.com.vn/wp-content/themes/icheck/assets/images/footer_qr.png"  class="qrcode"/>
                        </div>
                        <div class="get-app-store">
                            <a href="https://apps.apple.com/app/id1001036590" data-wpel-link="external" rel="nofollow external noopener noreferrer"><noscript><img src="https://icheck.com.vn/wp-content/themes/icheck/assets/images/app_store.svg" alt="" loading="lazy"></noscript><img class=" lazyloaded" src="https://icheck.com.vn/wp-content/themes/icheck/assets/images/app_store.svg" data-src="https://icheck.com.vn/wp-content/themes/icheck/assets/images/app_store.svg" alt="" loading="lazy"></a>
                            <a href="https://play.google.com/store/apps/details?id=vn.icheck.android&amp;hl=vi" data-wpel-link="external" rel="nofollow external noopener noreferrer"><noscript><img src="https://icheck.com.vn/wp-content/themes/icheck/assets/images/google_play.svg" alt="" loading="lazy"></noscript><img class=" lazyloaded" src="https://icheck.com.vn/wp-content/themes/icheck/assets/images/google_play.svg" data-src="https://icheck.com.vn/wp-content/themes/icheck/assets/images/google_play.svg" alt="" loading="lazy"></a>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-lg-7 footer-right-wrapper">
                    <ul class="address">
                        <li>
                            <p class="office">Văn phòng miền Bắc</p>
                            <p>Tầng 12 Tòa nhà Diamond Flower, số 48 Lê Văn Lương, Khu đô thị mới N1, phường Nhân Chính, quận Thanh Xuân, Hà Nội</p>
                        </li>
                        <li>
                            <p class="office">Văn phòng Hồ Chí Minh</p>
                            <p>Số 7/1 Thành Thái phường 14 quận 10, Tp Hồ Chí Minh</p>
                        </li>
                    </ul>
                    <div class="contact">
                        <span>
                            <i class="fa fa-phone" aria-hidden="true"></i>0902 195 488 - 0974 195 488
                        </span>
                        <span>
                            <i class="fa fa-envelope" aria-hidden="true"></i>cskh@icheck.com.vn
                        </span>
                    </div>
                    <div class="get-app-store-bottom">
                        <a href="https://apps.apple.com/app/id1001036590" data-wpel-link="external" rel="nofollow external noopener noreferrer"><noscript><img src="https://icheck.com.vn/wp-content/themes/icheck/assets/images/app_store.svg" alt="" loading="lazy"></noscript><img class=" lazyloaded" src="https://icheck.com.vn/wp-content/themes/icheck/assets/images/app_store.svg" data-src="https://icheck.com.vn/wp-content/themes/icheck/assets/images/app_store.svg" alt="" loading="lazy"></a>
                        <a href="https://play.google.com/store/apps/details?id=vn.icheck.android&amp;hl=vi" data-wpel-link="external" rel="nofollow external noopener noreferrer"><noscript><img src="https://icheck.com.vn/wp-content/themes/icheck/assets/images/google_play.svg" alt="" loading="lazy"></noscript><img class=" lazyloaded" src="https://icheck.com.vn/wp-content/themes/icheck/assets/images/google_play.svg" data-src="https://icheck.com.vn/wp-content/themes/icheck/assets/images/google_play.svg" alt="" loading="lazy"></a>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
<?php $otp_step = $this->session->flashdata('otp_step'); ?>
<?php if($otp_step): ?>
	<div class="modal fade" id="otp_confirm">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header" style="background-color: #238A3E;">
					<h4 class="modal-title" style="color: #fff;">Xác nhận đăng ký</h4>
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button> 
				</div>
				
				<div class="modal-body">
					<div class="row">
						<p style="width: 80%;text-align: center;margin: 0 auto;margin-bottom: 20px;">
							OTP đã được gửi tới số điện thoại của bạn. Nhập OTP để xác nhận đăng ký
						</p>
						<form class="form-inline" style="width: 100%;" method="post" action="<?php echo base_url('checkin') ?>">
						<div style="margin: 0 auto;margin-bottom: 20px; width: 100%;text-align: center;">
							<input class="form-control" name="otp" placeholder="Nhập OTP" style="margin-right: 15px;max-width: 60%;margin: 0 auto;">
							<button type="submit" name="confirm_otp" class="btn btn-default" style="background-color: #238A3E; color: #fff;">Xác nhận</button> 
						</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php endif; ?>

	<script src="<?php echo base_url('assets/js/owl_carousel/owl.carousel.min.js?v=' . VERSION_WEB) ?>"></script> 
	<script src="<?php echo base_url('assets/js/bootstrap.min.js?v=' . VERSION_WEB) ?>" id="script-resource-3"></script> 
	<script src="<?php echo base_url('assets/js/bootstrap-notify.min.js?v=' . VERSION_WEB) ?>"></script>
	<?php $error = $this->session->flashdata('error');
		if($error){
			$message = $error;
		}
		$success = $this->session->flashdata('success');
		if($success){
			$message = $success;
		}
		if(isset($message)){
			$message = str_replace("\n", '', $message);
		}
		
	?>
	<script>
    $(document).ready(function(){
		<?php if($otp_step): ?>
		$('#otp_confirm').modal({backdrop: 'static', keyboard: false});
		<?php endif; ?>
        //$('.cf7-frm-wrapper .wpcf7-submit').on('click',function(event){
            event.preventDefault();
            //var target = $(event.target);
            //var data = {
            //    "phoneNumber": $('.wpcf7-form-control-wrap input[name="sodienthoai"]').val(),
            //    "password": $('.wpcf7-form-control-wrap input[name="password"]').val(),
            //    "confirmPassword": $('.wpcf7-form-control-wrap input[name="check-password"]').val(),
            //    "firstName": $('.wpcf7-form-control-wrap input[name="company"]').val(),
            //    "lastName": $('.wpcf7-form-control-wrap input[name="company"]').val(),
            //    "tax": $('.wpcf7-form-control-wrap input[name="tax"]').val()};
            //console.log(data);
			//
            //$.ajax({
            //    type: "post",
            //    dataType: "json",
            //    contentType: 'application/json',
            //    url: "https://qrmkt-api.dev.icheck.vn/qr-code-free/api/v1/landing-page/register/request",
            //    data:JSON.stringify(data),
            //    success: function (response) {
            //        if (response.code == 1) {
            //            $('.cf7-frm-wrapper form').submit();
            //            setTimeout(function(){ window.location.replace("<?php echo site_url('/success/') ?>"); }, 500);
            //        } else {
            //            alert(response.message)
            //        }
            //    },
            //})
        //})
		
		<?php if($error && !isset($otp_step)): ?>
			$([document.documentElement, document.body]).animate({
				scrollTop: $("#form-register").offset().top
			}, 1500);
		<?php endif; ?>
		
		<?php if(!empty($message)): ?>
			
		$.notify({
			icon: 'glyphicon glyphicon-bell',
			message: <?php echo "'" . $message . "',"; ?>
			title: 'Thông báo',
		},{
			type:  <?php echo ($error)?"'danger'":"'success'" ?>,
			delay: 5000,
		});
		<?php endif; ?>
		
		
		setTimeout(function(){ $(location).attr('href', <?php echo "'" . base_url() . "'" ?>); }, 10000);

    })
	</script>
	
	
	<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-WFK9B3D');</script>
<!-- End Google Tag Manager -->

<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-WFK9B3D"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
</body>
</html>


