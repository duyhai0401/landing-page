<!DOCTYPE html> 
<html lang="en">
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta charset="utf-8">
		
		<title><?php echo WEB_TITLE ?></title>
		<link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url('assets/images/icon-title.jpg') ?>" />
		
		<link rel="stylesheet" href="//fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic" id="style-resource-3">
		<link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap.css?v=' . VERSION_WEB)    ?>" id="style-resource-4">
		
		<link rel="stylesheet" href="<?php echo base_url('assets/css/custom.css?v=' . VERSION_WEB) ?>" id="style-resource-8">
		<link rel="stylesheet" href="<?php echo base_url('assets/js/owl_carousel/owl.carousel.min.css?v=' . VERSION_WEB)  ?>">
		<link rel="stylesheet" href="<?php echo base_url('assets/css/animate.min.css?v=' . VERSION_WEB) ?>">
		<script src="<?php echo base_url('assets/js/jquery-1.11.3.min.js?v=' . VERSION_WEB)  ?>"></script> 
	  
	</head>
<body class="page-body page-fade" >
	<div class="container-fluid header-bg" style="padding-left: 0;padding-right: 0;">
		
			<div class="container">
				<div class="row" >
					<div class="col-md-2 col-sm-2">
						<img class="header-logo" src="<?php echo base_url() . 'assets/images/logo.png' ?>" />
					</div>
					<div class="col-md-10 col-sm-10 text-right">
						<a class="qr-tutorial" >Hướng dẫn quét mã check-in</a>
						<button class="btn icheck-btn destination-lookup">Tra cứu lịch sử Check-in</button>
					</div>
				</div>
				
				<div class="row" >
					<div class="col-md-5 col-sm-12 checkin">
						<div><p class="checkin-title-1">Check in ngay, </p><div class="title-line"></div></div>
						<p class="checkin-title-2">nhận quà liền tay</p>
						<ul style="padding-left: 20px;">
							<li>Check in bằng mã QR tại điểm đến, đảm bảo an toàn mùa dịch <br>
							<li>Không giới hạn cơ hội nhận quà khi check-in </li>
						</ul>
					</div>
					<div class="col-md-7 col-sm-12">
						<img class="checkin-qr-img" src="<?php echo base_url() . 'assets/images/checkin-qr-img.png' ?>" />
					</div>
				</div>
				
				
			</div>
		
		<div id="fixedContainer"></div>
		
	</div>
	
	<div class="container-fluid" style="padding-top: 80px;">
		<div class="container">
			<div class="row ">
				<div class="col-md-6 col-sm-12 col-md-offset-3 clearfix advantages-checkin" >Vì sao cần quét mã QR check-in?</div>
			</div>
			<div class="row ">
				<div class="col-12 bg-advantages-title"></div>
			</div>
			<div class="row " style="margin-top: 50px;">
				<div class="col-md-3 col-sm-6" >
					<div class="advantages-item-icon advantages-icon-1"></div>
					<div class="advantages-item-text">Hỗ trợ cơ quan chức năng nắm bắt thông tin để kiểm soát vùng dịch và truy vết đối tượng nhiễm dịch khi cần</div>
				</div>
				<div class="col-md-3 col-sm-6" style="margin-bottom: 60px;">
					<div class="advantages-item-icon advantages-icon-2"></div>
					<div class="advantages-item-text">Có cơ hội nhận các phần quà tri ân khách hàng từ doanh nghiệp	</div>
				</div>
				<div class="col-md-3 col-sm-6" >
					<div class="advantages-item-icon advantages-icon-3"></div>
					<div class="advantages-item-text">Tự tra cứu lại danh sách điểm đã tới để đối chiếu khi cần khai báo với cơ quan chức năng</div>
				</div>
				<div class="col-md-3 col-sm-6" style="margin-bottom: 60px;">
					<div class="advantages-item-icon advantages-icon-4"></div>
					<div class="advantages-item-text">Bảo vệ cộng đồng, mỗi cá nhân tự ý thức về trách nhiệm khai báo với mỗi điểm đi qua</div>
				</div>
			</div>
			
		</div>
	</div>
	
	<div id="tutorial-qr-checkin" class="container-fluid" style="padding-top: 80px;background: linear-gradient(180deg, #F7F7F7 0%, rgba(255, 255, 255, 0) 32.81%);position: relative;">
		<div class="bg-virus"></div>
		<div class="container">
			<div class="row tutor-san-qr">
				<div class="col-md-12 col-sm-12">
					
					<div class="row " >
						
						
						<div class="col-md-6" style="position: relative;">
							<div class="tutor-san-qr-title" >
								Quét mã QR check-in như thế nào?
							</div>					
							<div class="col-12 bg-advantages-title" style="margin-top: 21px;background-position-x: -10px;"></div>
							
							<div class="img " style="padding-top: 40px;">
								<div class="tutor-step tutor-active"><span class="step-1">Bước 1</span></div>
								<span class="text-dot" style="top: 167px;left: 138px;">Tìm mã QR check-in của iCheck tại điểm đến</span>
							</div>
							<div class="img " >
								<div class="tutor-step tutor-unactive"><span class="step-2">Bước 2</span></div>
								<span class="text-dot" style="top: 300px;left: 138px;">Quét mã bằng camera hoặc bất kỳ ứng dụng quét mã QR nào bạn có sẵn</span>
							</div>
							<div class="img " >
								<div class="tutor-step tutor-unactive"><span class="step-3">Bước 3</span></div>
								<span class="text-dot" style="top: 447px;left: 138px;">Để lại thông tin để doanh nghiệp thông báo khi cần thiết</span>
							</div>
							<div class="img " >
								<div class="tutor-step tutor-last"><span class="step-4">Bước 4</span></div>
								<span class="text-dot" style="top: 580px;left: 138px;">Tham gia vòng quay may mắn để có cơ hội nhận quà từ doanh nghiệp (Tùy vào chương trình của doanh nghiệp)</span>
							</div>
							
							
							<!--div class="featured-carousel owl-carousel">
								
								<div class="item">
									<div class="work-wrap d-md-flex">
										<div class="img order-md-last" style="background-repeat: no-repeat;background-image: url(<?php echo base_url() . 'assets/images/icon-phone.png' ?>);"></div>
										
									</div>
								</div>
								
								<div class="item">
									<div class="work-wrap d-md-flex">
										<div class="img order-md-last" style="background-repeat: no-repeat;background-image: url(<?php echo base_url() . 'assets/images/icon-mail.png' ?>);"></div>
										
									</div>
								</div>
								
							</div-->
							
						</div>
						
						<div class="col-md-6 bg-phone" ></div>
					</div>
				</div>
				<div class="col-md-5 col-sm-12">
				</div>
			</div>
		</div>
	</div>
	
	<div id="section-login" class="container-fluid login" >
		<div class="container">
			<div class="row">
				<div class="col-md-7 col-md-offset-5">
					<div class="login-title">Đăng nhập bằng số điện thoại để tra cứu các địa điểm bạn đã đến và quét mã QR check-in</div>
					<div class="bg-login-title"></div>
					
					<?php $otp_step = $this->session->flashdata('otp_step'); ?>
					
					<form method="post" action="<?php echo base_url('qr-code-checkin') ?>" class="login-form">
						<div class="input-group" style="width: 90%;margin: 0 auto;">
							<?php if(!$otp_step): ?>
								<input type="text" class="form-control" name="phone" placeholder="Nhập số điện thoại đã đăng ký khi quét mã">
								<div class="input-group-addon button-otp" ><button class="btn" name="get_otp" type="submit">Nhận mã OTP</button></div>
							<?php else: ?>
								<input type="text" class="form-control" name="otp" placeholder="Nhập mã OTP">
								<div class="input-group-addon button-otp" ><button class="btn" name="confirm_login" type="submit">Xác nhận</button></div>
							<?php endif; ?>
						</div>
					</form>
					
					
				</div>
			</div>
		</div>
	</div>
	
	<div class="container-fluid footer" >
		<div class="container">
			<div class="row">
				<div class="col-md-6 col-sm-6">
					<div class="logo-footer"></div>
					<p class="icheck-name">Công ty cổ phần iCheck</p>
					
					<div class="col-md-6 col-sm-6 footer-qrcode">
					</div>
					
					<div class="col-md-6 col-sm-6">
						<div class="appstore"></div>
						<div class="ggplay"></div>
					</div>
					
				</div>
				<div class="col-md-6 col-sm-6">
					<p class="branch-name">Văn phòng miền Bắc</p>
					<div class="branch-address">
						<div class="icon-location" ></div>
						<p>Tầng 12 Tòa nhà Diamond Flower, số 48 Lê Văn Lương, Khu đô thị mới N1, Phường Nhân Chính, Quận Thanh Xuân, Hà Nội</p>
					</div>
					
					<p class="branch-name" style="padding-top: 25px;">Văn phòng miền Nam</p>
					<div class="branch-address">
						<div class="icon-location" style="width: 27px;"></div>
						<p>Số 7/1 Thành Thái phường 14 quận 10, Tp Hồ Chí Minh</p>
					</div>
					
					<div class="contact-info" >
						<p class="phone-mail-contect"><img style="margin-right: 12px;" src="<?php echo base_url() . 'assets/images/icon-phone.png' ?>"></i>0902 195 488 - 0974 195 488</p>
						<p class="phone-mail-contect"><img style="margin-right: 12px;" src="<?php echo base_url() . 'assets/images/icon-mail.png' ?>"></i>cskh@icheck.com.vn</p>
					</div>
					
				</div>
			</div>
		</div>
		
		<div class="bg-virus-right"></div>
	</div>
	  
	  
	<script src="<?php echo base_url('assets/js/bootstrap.js?v=' . VERSION_WEB) ?>" id="script-resource-3"></script> 
	<script src="<?php echo base_url('assets/js/jquery-ui/js/jquery-ui-1.10.3.minimal.min.js?v=' . VERSION_WEB) ?>" id="script-resource-2"></script> 
	<script src="<?php echo base_url('assets/js/owl_carousel/owl.carousel.min.js?v=' . VERSION_WEB) ?>"></script>
	<script src="<?php echo base_url('assets/js/bootstrap-notify.min.js?v=' . VERSION_WEB) ?>"></script>
	
	<?php $error = $this->session->flashdata('error');
		if($error){
			$message = $error;
		}
		$success = $this->session->flashdata('success');
		if($success){
			$message = $success;
		}
		if(isset($message)){
			$message = str_replace("\n", '', $message);
		}
	?>
	<script>
		$(document).ready(function() {
			/*
			$('.featured-carousel').owlCarousel({
				loop:true,
				autoplay: true,
				margin:30,
				animateOut: 'fadeOut',
				animateIn: 'fadeIn',
				nav:false,
				dots: true,
				autoplayHoverPause: false,
				items: 1,
				
				responsive:{
				  0:{
					items:1
				  },
				  600:{
					items:1
				  },
				  1000:{
					items:1
				  }
				}
			});
			*/
			
			$(".qr-tutorial").click(function() {
				$([document.documentElement, document.body]).animate({
					scrollTop: $("#tutorial-qr-checkin").offset().top
				}, 1500);
			});
			
			$(".destination-lookup").click(function() {
				$([document.documentElement, document.body]).animate({
					scrollTop: $("#section-login").offset().top
				}, 1500);
			});
			
			<?php if(!empty($message)): ?>
			$([document.documentElement, document.body]).animate({
					scrollTop: $("#section-login").offset().top
				}, 1500);
			$.notify({
				icon: 'glyphicon glyphicon-bell',
				message: <?php echo "'" . $message . "',"; ?>
				title: 'Thông báo',
			},{
				type:  <?php echo ($error)?"'danger'":"'success'" ?>,
				delay: 5000,
			});
			<?php endif; ?>
		});
	</script>
	
	
</body>
</html>