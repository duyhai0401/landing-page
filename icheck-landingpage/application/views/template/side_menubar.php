<?php 
	//$ci =& get_instance();
	$session_data = $this->session->userdata();
	$path = $this->uri->segment(1);
	if(empty($path)){
		$path = 'home';
	}
?>			
		<div class="sidebar-menu">
			<div class="sidebar-menu-inner">
				<header class="logo-env">
					<div class="logo"> <a href="/home"> <img src="/assets/images/logo@2x.png" width="120" alt="" /> </a> </div>
					<div class="sidebar-collapse"> <a href="#" class="sidebar-collapse-icon"> <i class="entypo-menu"></i> </a> </div>
					<div class="sidebar-mobile-menu visible-xs"> <a href="#" class="with-animation"> <i class="entypo-menu"></i> </a> </div>
				</header>
				<ul id="main-menu" class="main-menu">
					<?php foreach($menus as $menu): ?>
						<?php $active = array_search($path, array_column($menu['sub_permission'], 'alias')); ?>
						<?php ?>
							<li class="<?php echo ($active!== false)?'active opened ':'' ?> has-sub">
								<a href=""><i class="<?php echo $menu['icon'] ?>"></i><span class="title"><?php echo $menu['name'] ?></span></a> 
								<ul>
									<?php foreach($menu['sub_permission'] as $sub_menu): ?>
										<li > 
											<a href="<?php echo base_url() . $sub_menu['alias'] ?>" class="<?php echo ($sub_menu['alias'] == $path)?'active':'' ?>">
												<i class="<?php echo $sub_menu['icon'] ?>"></i>
												<span class="title"><?php echo $sub_menu['name'] ?></span>
											</a> 
										</li>
									<?php endforeach; ?>
								</ul>
							</li>
						<?php ?>
					<?php endforeach; ?>
				</ul>
			</div>
		</div>

		<div class="main-content">
			<!-- top menu -->
			<div class="row">
				<div class="col-md-6 col-sm-8 clearfix">
					<ul class="user-info pull-left pull-none-xsm">
						<li class="profile-info dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown"> <img src="/assets/images/thumb-1@2x.png" alt="" class="img-circle" width="44" />
							<?php echo $session_data['name']; ?>
							</a> 
							<ul class="dropdown-menu">
							<li class="caret"></li>
							<li> 
								<a href="<?php echo base_url('doi-mat-khau') ?>"> <i class="entypo-key"></i>
								Đổi mật khẩu
								</a> 
							</li>
							<!--li> <a href="/mailbox/main/"> <i class="entypo-mail"></i>
								Inbox
								</a> 
							</li>
							<li> <a href="/extra/calendar/"> <i class="entypo-calendar"></i>
								Calendar
								</a> 
							</li>
							<li> <a href="#"> <i class="entypo-clipboard"></i>
								Tasks
								</a> 
							</li-->
							</ul>
						</li>
					</ul>
					<ul class="user-info pull-left pull-right-xs pull-none-xsm">
						<li class="notifications dropdown">
								<?php 
									$ci =& get_instance();
									$ci->load->library('fin_api');
									$limit = 10;
									$offset = 0;
									$get_list_inbox = $ci->fin_api->get_list_inbox($session_data['email'], $session_data['token'], $limit, $offset);
									$list_inbox = $get_list_inbox->inbox;
									$count_inbox = 0;
								?>
							<a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true"> <i class="entypo-mail"></i> 
								<?php if(isset($get_list_inbox->totalUInread) && $get_list_inbox->totalUInread > 0): ?>
								<span id="total_unread" class="badge badge-secondary"><?php echo $get_list_inbox->totalUInread; ?></span> 
								<?php endif; ?>
							</a> 
							<ul class="dropdown-menu">
								
								<li>
									<ul id="inbox-list" class="dropdown-menu-list scroller">
									<?php if($list_inbox): ?>
									<?php foreach($list_inbox as $key=> $inbox): ?>
									<?php $count_inbox++ ?>
										<li data-inbox_id="<?php echo $inbox->inboxId ?>" class="active" <?php if(!isset($inbox->inboxRead) || $inbox->inboxRead==0): ?> onclick="readedInbox(this)" <?php endif ?>> 
											<div class="menu-div"> 
												<?php $notify_path = "" ;
													if(isset($inbox->loanCode) && !empty($inbox->loanCode)){
														if(in_array($inbox->loanStatus, ['107', '109', '115', '125'])){
															$notify_path = 'hop-dong-vay/chi-tiet-khoan-vay/' . $inbox->loanCode;
														} elseif(in_array($inbox->loanStatus, ['108', '111'])){
															$notify_path = 'tham-dinh-ho-so/chi-tiet-khoan-vay/' . $inbox->loanCode;
														} elseif(in_array($inbox->loanStatus, ['113', '116', '117'])){
															$notify_path = 'giai-ngan/chi-tiet-khoan-vay/' . $inbox->loanCode;
														} elseif(in_array($inbox->loanStatus, ['120', '121'])){
															$notify_path = 'nhac-no/chi-tiet-khoan-vay/' . $inbox->loanCode;
														}
													}
												?>
												<?php if(!empty($notify_path)): ?>
												<a href="<?php echo base_url($notify_path); ?>">
												<?php endif ?>
													<?php if(!isset($inbox->inboxRead)): ?>
													<span class="pull-right unread_inbox unread_<?php echo $inbox->inboxId ?>"> </span>
													<?php elseif($inbox->inboxRead == 0): ?>
													<span class="pull-right unread_inbox unread_<?php echo $inbox->inboxId ?>"> </span>
													<?php endif; ?>
													
													<span class="line"> 
														<strong>Hợp đồng cần xử lý</strong>
													</span> 
													<span class="line desc small">
														<?php echo $inbox->content ?>
													</span> 
												<?php if(!empty($notify_path)): ?>
												</a>
												<?php endif ?>
											</div> 
										</li>
									<?php endforeach; ?>
									<?php endif; ?>	
									</ul>
								</li>
							
								<?php if($count_inbox==10): ?>
								<li class="external">
									<a href="javascript:;" id="read_more_message" data-load_more="2" onclick="loadMoreInbox(this)">Xem thêm</a>
									<!--a id="read_more_message" data-load_more="2" >Xem thêm</a-->
								</li>
								<?php endif; ?>
							</ul>
						</li>
					</ul>
				</div>
				<div class="col-md-6 col-sm-4 clearfix hidden-xs">
					<ul class="list-inline links-list pull-right">
						<!--li class="dropdown language-selector">
							Language: &nbsp;
							<a href="#" class="dropdown-toggle" data-toggle="dropdown" data-close-others="true"> <img src="/assets/images/flags/flag-uk.png" width="16" height="16" /> </a> 
							<ul class="dropdown-menu pull-right">
							<li> <a href="#"> <img src="/assets/images/flags/flag-de.png" width="16" height="16" /> <span>Deutsch</span> </a> </li>
							<li class="active"> <a href="#"> <img src="/assets/images/flags/flag-uk.png" width="16" height="16" /> <span>English</span> </a> </li>
							<li> <a href="#"> <img src="/assets/images/flags/flag-fr.png" width="16" height="16" /> <span>François</span> </a> </li>
							<li> <a href="#"> <img src="/assets/images/flags/flag-al.png" width="16" height="16" /> <span>Shqip</span> </a> </li>
							<li> <a href="#"> <img src="/assets/images/flags/flag-es.png" width="16" height="16" /> <span>Español</span> </a> </li>
							</ul>
						</li>
						<li class="sep"></li>
						<li> <a href="#" data-toggle="chat" data-collapse-sidebar="1"> <i class="entypo-chat"></i>
							Chat
							<span class="badge badge-success chat-notifications-badge is-hidden">0</span> </a> 
						</li>
						<li class="sep"></li-->
						<li> <a href="<?php echo base_url('dang-xuat'); ?>">
							Log Out <i class="entypo-logout right"></i> </a> 
						</li>
					</ul>
				</div>
			</div>
				<hr />
				<!-- end top menu -->
				<script>
					/*
					function loadMoreInbox(identifier){
						$(identifier).css('display', 'none');
						event.stopPropagation();
						var offset = $(identifier).data('load_more');
						var fd = new FormData();
						fd.append('offset',offset);
						$.ajax({
							url: 'load-more-inbox',
							type: 'post',
							data: fd,
							contentType: false,
							processData: false,
							dataType: "json",
							success: function(response){
								if(response.status){
									$('#inbox-list').append(response.html_inbox);
									if(response.disabled_load_more){
										$(identifier).css('display', 'none');
									} else {
										$(identifier).data('load_more', offset+1);
										$(identifier).css('display', 'block');
									}
								}
							}
						});
					}
					*/
					
					$(document).ready(function() {
						
						/*
						$('body').on('click','.inbox-item',function(e){
							e.preventDefault();
							console.log('click inbox-item');
							var inbox_id = $(this).data('inbox_id');
							$('.unread_' + inbox_id).css('display', 'none');
							$('#total_unread').html($('#total_unread').html() - 1);
							
							var fd = new FormData();
							fd.append('inbox_id',inbox_id);
							$.ajax({
								url: '/readed-inbox',
								type: 'post',
								data: fd,
								contentType: false,
								processData: false,
								dataType: "json",
								success: function(response){
									
								}
							});
							
						});
						*/
						$('body').on('click','.inbox-item',function(e){
							e.preventDefault();
							console.log('click inbox-item');
						})
						
					});
					
				</script>