<!DOCTYPE html> 
<html lang="en">
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<meta name="description" content="Admin Panel" />
		<meta name="author" content="windmonkey" />
		<link rel="icon" href="/assets/images/favicon.ico">
		
		<title>Fintech | <?php echo (isset($page_title))?$page_title:'' ?></title>
		<link rel="stylesheet" href="<?php echo base_url('/assets/js/jquery-ui/css/no-theme/jquery-ui-1.10.3.custom.min.css') . '?v=' . VERSION_WEB; ?>" id="style-resource-1">
		<link rel="stylesheet" href="<?php echo base_url('/assets/css/font-icons/entypo/css/entypo.css') . '?v=' . VERSION_WEB; ?>" id="style-resource-2">
		<link rel="stylesheet" href="//fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic" id="style-resource-3">
		<link rel="stylesheet" href="<?php echo base_url('/assets/css/bootstrap.css') . '?v=' . VERSION_WEB; ?>" id="style-resource-4">
		<link rel="stylesheet" href="<?php echo base_url('/assets/css/fintech-core.css') . '?v=' . VERSION_WEB; ?>" id="style-resource-5">
		<link rel="stylesheet" href="<?php echo base_url('/assets/css/fintech-theme.css') . '?v=' . VERSION_WEB; ?>" id="style-resource-6">
		<link rel="stylesheet" href="<?php echo base_url('/assets/css/fintech-forms.css') . '?v=' . VERSION_WEB; ?>" id="style-resource-7">
		<link rel="stylesheet" href="<?php echo base_url('/assets/css/custom.css') . '?v=' . VERSION_WEB; ?>" id="style-resource-8">
		<link rel="stylesheet" href="<?php echo base_url('/assets/css/skins/facebook.css') . '?v=' . VERSION_WEB; ?>" id="style-resource-9">
		<script src="<?php echo base_url('/assets/js/jquery-1.11.3.min.js') . '?v=' . VERSION_WEB; ?>"></script> 
		<script src="<?php echo base_url('/assets/js/jquery.validate.min.js') . '?v=' . VERSION_WEB; ?>" id="script-resource-8"></script>
		<link rel="stylesheet" href="<?php echo base_url('/assets/js/select2/select2-bootstrap.css') . '?v=' . VERSION_WEB; ?>" id="style-resource-1">
		<link rel="stylesheet" href="<?php echo base_url('/assets/js/select2/select2.css') . '?v=' . VERSION_WEB; ?>" id="style-resource-2">
		
	</head>
	<body class="page-body skin-facebook loaded" data-url="">
		<div class="page-container">
			