		
		</div>
			
		</div>
		<footer class="main">
           <div class="pull-right"> WM </div>
           &copy; 2021 <strong>Fintech</strong> 
        </footer>
		
		
		<link rel="stylesheet" href="<?php echo base_url('/assets/js/zurb-responsive-tables/responsive-tables.css') . '?v=' . VERSION_WEB; ?>" id="style-resource-1">
		<link rel="stylesheet" href="<?php echo base_url('/assets/js/jvectormap/jquery-jvectormap-1.2.2.css') . '?v=' . VERSION_WEB; ?>" id="style-resource-1">
		<link rel="stylesheet" href="<?php echo base_url('/assets/js/rickshaw/rickshaw.min.css') . '?v=' . VERSION_WEB; ?>" id="style-resource-2">
		<link rel="stylesheet" href="<?php echo base_url('/assets/loading.css') . '?v=' . VERSION_WEB; ?>" >
		
		<script src="<?php echo base_url('/assets/js/gsap/TweenMax.min.js') . '?v=' . VERSION_WEB; ?>" id="script-resource-1"></script> 
		<script src="<?php echo base_url('/assets/js/jquery-ui/js/jquery-ui-1.10.3.minimal.min.js') . '?v=' . VERSION_WEB; ?>" id="script-resource-2"></script> 
		<script src="<?php echo base_url('/assets/js/bootstrap.js') . '?v=' . VERSION_WEB; ?>" id="script-resource-3"></script> 
		<script src="<?php echo base_url('/assets/js/joinable.js') . '?v=' . VERSION_WEB; ?>" id="script-resource-4"></script> 
		<script src="<?php echo base_url('/assets/js/resizeable.js') . '?v=' . VERSION_WEB; ?>" id="script-resource-5"></script> 
		<script src="<?php echo base_url('/assets/js/fintech-api.js') . '?v=' . VERSION_WEB; ?>" id="script-resource-6"></script> 
		<!--script src="/assets/js/cookies.min.js" id="script-resource-7"></script--> 
		<script src="<?php echo base_url('/assets/js/jvectormap/jquery-jvectormap-1.2.2.min.js') . '?v=' . VERSION_WEB; ?>" id="script-resource-8"></script> 
		<script src="<?php echo base_url('/assets/js/jvectormap/jquery-jvectormap-europe-merc-en.js') . '?v=' . VERSION_WEB; ?>" id="script-resource-9"></script> 
		<script src="<?php echo base_url('/assets/js/jquery.sparkline.min.js') . '?v=' . VERSION_WEB; ?>" id="script-resource-10"></script> 
		<script src="<?php echo base_url('/assets/js/rickshaw/vendor/d3.v3.js') . '?v=' . VERSION_WEB; ?>" id="script-resource-11"></script> 
		<script src="<?php echo base_url('/assets/js/rickshaw/rickshaw.min.js') . '?v=' . VERSION_WEB; ?>" id="script-resource-12"></script> 
		<script src="<?php echo base_url('/assets/js/raphael-min.js') . '?v=' . VERSION_WEB; ?>" id="script-resource-13"></script> 
		<script src="<?php echo base_url('/assets/js/morris.min.js') . '?v=' . VERSION_WEB; ?>" id="script-resource-14"></script> 
		<script src="<?php echo base_url('/assets/js/toastr.js') . '?v=' . VERSION_WEB; ?>" id="script-resource-15"></script> 
		<script src="<?php echo base_url('/assets/js/fintech-chat.js') . '?v=' . VERSION_WEB; ?>" id="script-resource-16"></script>  
		<script src="<?php echo base_url('/assets/js/fintech-custom.js') . '?v=' . VERSION_WEB; ?>" id="script-resource-17"></script>  
		<script src="<?php echo base_url('/assets/js/fintech-demo.js') . '?v=' . VERSION_WEB; ?>" id="script-resource-18"></script> 
		<script src="<?php echo base_url('/assets/js/fintech-skins.js') . '?v=' . VERSION_WEB; ?>" id="script-resource-19"></script> 
		<script src="<?php echo base_url('/assets/js/zurb-responsive-tables/responsive-tables.js') . '?v=' . VERSION_WEB; ?>" id="script-resource-20"></script>
		<script src="<?php echo base_url('/assets/js/bootstrap-datepicker.js') . '?v=' . VERSION_WEB; ?>" id="script-resource-21"></script> 
		<script src="<?php echo base_url('/assets/js/select2/select2.min.js') . '?v=' . VERSION_WEB; ?>" id="script-resource-22"></script>
		<script src="<?php echo base_url('/assets/jquery.loading.js') . '?v=' . VERSION_WEB; ?>" id="script-resource-23"></script>
		 
	</body>
</html>